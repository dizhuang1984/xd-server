package com.rh.soc.appmon.controller;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.rh.soc.appmon.model.AppStatusBean;
import com.rh.soc.watch.appmonitor.model.AppSystemVo;
import com.rh.soc.watch.appmonitor.service.IAppMonitorService;
import com.rh.webserver.common.base.bean.AppResultBean;
import com.rh.webserver.common.base.controller.BaseController;

@Controller
@RequestMapping("/third/appmon")
public class AppMonControllerNew extends BaseController {

	private Logger logger = Logger.getLogger(AppMonControllerNew.class);

	@Resource
	private IAppMonitorService SERVICE;

	@RequestMapping("queryAppSystemThumbnail")
	@ResponseBody
	public AppResultBean queryAppSystemThumbnail(@RequestParam(value = "param", required = false) String param,
			HttpSession session) throws Exception {
		AppResultBean bean = new AppResultBean();
		try {
			AppSystemVo vo = new AppSystemVo();
			vo.setPageSize(1000);
			vo.setCurrentPageNum(1);
			// 查询出所有应用系统的缩略图（包括当前的应用系统状态）
			vo.setUserId(getUserId(session));
			List<AppSystemVo> list = SERVICE.queryAppSystemThumbnail(vo);
			List<AppStatusBean> results = new ArrayList<>();
			if (list != null) {
				for (AppSystemVo vox : list) {
					AppStatusBean r = new AppStatusBean();
					r.setAppId(vox.getAppId());
					r.setAppName(vox.getAppName());
					r.setFaultCompNum(vox.getFaultCompNum());
					r.setNormalCompNum(vox.getNormalCompNum());
					r.setPerformCompNum(vox.getPerformCompNum());
					r.setUnknownCompNum(vox.getUnknownCompNum());
					r.setAuthCompNum(vox.getAuthCompNum());
					r.setAppStatus(Integer.parseInt(vox.getAppStatus()));
					results.add(r);
				}
			}
			bean.setObj(results);
		} catch (Exception e) {
			logger.error("获取数据失败", e);
			bean.fail("获取数据失败");
		}
		return bean;
	}

	@RequestMapping("queryAppSystemThumbnailSummary")
	@ResponseBody
	public AppResultBean queryAppSystemThumbnailSummary(@RequestParam(value = "param", required = false) String param,
			HttpSession session) throws Exception {
		AppResultBean bean = new AppResultBean();
		try {
			AppSystemVo vo = new AppSystemVo();
			vo.setPageSize(1000);
			vo.setCurrentPageNum(1);
			// 查询出所有应用系统的缩略图（包括当前的应用系统状态）
			vo.setUserId(getUserId(session));
			List<AppSystemVo> list = SERVICE.queryAppSystemThumbnail(vo);
			int good = 0;
			int nodata = 0;
			int perf = 0;
			int fault = 0;
			int auth = 0;
			if (list != null) {
				for (AppSystemVo vox : list) {
					AppStatusBean r = new AppStatusBean();
					int n = Integer.parseInt(vox.getAppStatus());
					r.setAppStatus(Integer.parseInt(vox.getAppStatus()));
					if (n == 0) {
						fault++;
					} else if (n == 1) {
						perf++;
					} else if (n == 2) {
						good++;
					} else if (n == -2) {
						auth++;
					} else {
						nodata++;
					}
				}
			}
			bean.put("good", good);
			bean.put("nodata", nodata);
			bean.put("perf", perf);
			bean.put("fault", fault);
			bean.put("auth", auth);
		} catch (Exception e) {
			logger.error("获取数据失败", e);
			bean.fail("获取数据失败");
		}
		return bean;
	}
}
