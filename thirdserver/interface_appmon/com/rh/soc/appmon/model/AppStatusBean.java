package com.rh.soc.appmon.model;

public class AppStatusBean {

	private String appId;
	private String appName;
	private int appStatus;

	private int faultCompNum = 0;
	private int performCompNum = 0;
	private int normalCompNum = 0;
	private int unknownCompNum = 0;
	private int authCompNum = 0;

	public String getAppId() {
		return appId;
	}

	public void setAppId(String appId) {
		this.appId = appId;
	}

	public String getAppName() {
		return appName;
	}

	public void setAppName(String appName) {
		this.appName = appName;
	}

	public int getFaultCompNum() {
		return faultCompNum;
	}

	public void setFaultCompNum(int faultCompNum) {
		this.faultCompNum = faultCompNum;
	}

	public int getPerformCompNum() {
		return performCompNum;
	}

	public void setPerformCompNum(int performCompNum) {
		this.performCompNum = performCompNum;
	}

	public int getNormalCompNum() {
		return normalCompNum;
	}

	public void setNormalCompNum(int normalCompNum) {
		this.normalCompNum = normalCompNum;
	}

	public int getUnknownCompNum() {
		return unknownCompNum;
	}

	public void setUnknownCompNum(int unknownCompNum) {
		this.unknownCompNum = unknownCompNum;
	}

	public int getAuthCompNum() {
		return authCompNum;
	}

	public void setAuthCompNum(int authCompNum) {
		this.authCompNum = authCompNum;
	}

	public int getAppStatus() {
		return appStatus;
	}

	public void setAppStatus(int appStatus) {
		this.appStatus = appStatus;
	}
}
