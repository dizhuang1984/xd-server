package com.rh.soc.equipmenttypethirdinterface.service;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.web.multipart.MultipartFile;

import com.rh.soc.management.basedata.equipmenttype.model.EquipmentClassVO;
import com.rh.soc.management.basedata.equipmenttype.model.EquipmentTypeVO;
import com.rh.webserver.common.base.exception.AppException;

public interface IEquipmentTypeThirdService
{
    /**
     * 页面初始，查询所有数据；修改时根据id查询单一数据
     * 
     */

    public List<EquipmentTypeVO> queryEquipmentTypeData(EquipmentTypeVO vo) throws AppException;

    /**
     * 添加页面保存数据
     * 
     */
    public int addEquipmentTypeSave(HttpServletRequest request,EquipmentTypeVO vo) throws AppException;

    /**
     * 保存修改数据
     * 
     */
    public int updEquipmentTypeSave(HttpServletRequest request,EquipmentTypeVO vo) throws AppException;

    /**
     * 查询总类型数据
     * 
     */
    public Map<String, List<EquipmentClassVO>> queryClassType(EquipmentClassVO vo) throws AppException;

    /**
     * 删除选择数据
     * 
     */
    //public int delEquipmentType(EquipmentTypeVO vo, HttpServletRequest request) throws AppException;

    /**
     * 
     * 批量删除前查询是否可以删除
     * 
     */
	public List<EquipmentTypeVO> preBatchDel(List<EquipmentTypeVO> list) throws AppException;

	/**
	 * 
	 * 批量删除
	 * 
	 */
	public int delBatch(List<EquipmentTypeVO> list, HttpServletRequest request) throws AppException;
	
	/**
     * 
     * {同步资产类型}
     * 
     */
    //public void syncEquipmentType() throws AppException;
    
    /**
     * 查询资产类型（华能定制）
     * 
     */
    //public String queryAssetTypeForHuaneng() throws AppException;
	
	 public List<EquipmentClassVO> queryEquipmentClass(EquipmentClassVO vo) throws AppException;
    
}
