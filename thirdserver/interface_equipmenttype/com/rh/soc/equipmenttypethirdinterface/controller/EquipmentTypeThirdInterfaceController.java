package com.rh.soc.equipmenttypethirdinterface.controller;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.rh.soc.equipmenttypethirdinterface.service.IEquipmentTypeThirdService;
import com.rh.soc.management.basedata.equipmenttype.model.EquipmentTypeVO;
import com.rh.webserver.common.base.bean.AppResultBean;
import com.rh.webserver.common.base.controller.BaseController;
import com.rh.webserver.common.base.exception.AppException;
import com.rh.webserver.common.base.json.IDataCenter;
import com.rh.webserver.common.util.BeanUtils;
import com.rh.webserver.common.util.StringUtil;

@Controller
@RequestMapping("/third/EquipmentType")
public class EquipmentTypeThirdInterfaceController extends BaseController {
	private Logger logger = Logger.getLogger(error);

	@Resource
	IEquipmentTypeThirdService equipmentTypeService;

	/**
	 * 添加资产类型信息
	 * 
	 * 添加页面保存数据
	 * 
	 * @param param
	 * @param request
	 * @param session
	 * @return
	 * @throws Exception
	 */
	@ResponseBody
	@RequestMapping("addEquipmentType")
	public AppResultBean addEquipmentTypeSave(@RequestParam("param") String param, HttpSession session,HttpServletRequest request)
			throws Exception {
		logger.info("param=[" + param + "]");
 		AppResultBean bean = new AppResultBean();
		EquipmentTypeVO vo = new EquipmentTypeVO();
		IDataCenter idc = BeanUtils.toDataCenter(param);

		String pid = idc.getParameter("pid");
		String id = idc.getParameter("id");
		String name = idc.getParameter("name");
		String osType = idc.getParameter("osType");
		String deviceTypeMap = idc.getParameter("img");//;
		String deviceTypeMapContent = idc.getParameter("image");

		if (StringUtil.isEmpty(pid)) {
			bean.fail("资产类型大类编号不能为空");
			return bean;
		}
		if (StringUtil.isEmpty(id)) {
			bean.fail("资产类型编号不能为空");
			return bean;
		}
		if (StringUtil.isEmpty(name)) {
			bean.fail("资产类型名称不能为空");
			return bean;
		}
		if (StringUtil.isEmpty(osType)) {
			vo.setOsType("108");
		} else {
			vo.setOsType(osType);
		}
		
		
//		if(StringUtil.isNotEmpty(deviceTypeMap)&&StringUtil.isEmpty(deviceTypeMapContent)){
//			bean.fail("图片名称不为空，但是无图片");
//			return bean;
//		}

		vo.setDeviceClass(Long.parseLong(pid));
		vo.setDeviceType(id);
		vo.setDeviceTypeName(name);
		vo.setDeviceTypeMap(deviceTypeMap);
		vo.setDeviceTypeMapContent(deviceTypeMapContent);

		try {
			int rtn = equipmentTypeService.addEquipmentTypeSave(request,vo);
			switch (rtn) {
			case -1:
				bean.fail("添加资产类型同步失败");
				break;
			case 1:
				bean.ok("添加资产类型同步成功");
				break;
			case 2:
				bean.fail("该资产类型名已经存在");
				break;
			case 3:
				bean.fail("该资产类型编号已经存在");
				break;
			case 9:
				bean.fail("该资产类型大类编号不存在");
				break;
			}

		} catch (AppException e) {
			bean.fail("添加资产类型失败");
			logger.error("添加资产类型失败", e);
		}
		return bean;
	}

	/**
	 * 更新修改资产类型信息
	 * 
	 * 保存修改数据
	 * 
	 * @param param
	 * @param session
	 * @param request
	 * @return
	 * @throws Exception
	 */
	@ResponseBody
	@RequestMapping("updEquipmentType")
	public AppResultBean updEquipmentTypeSave(@RequestParam("param") String param, HttpSession session,HttpServletRequest request)
			throws Exception {
		logger.info("param=[" + param + "]");
		AppResultBean bean = new AppResultBean();
		EquipmentTypeVO vo = new EquipmentTypeVO();
		IDataCenter idc = BeanUtils.toDataCenter(param);
		String pid = idc.getParameter("pid");
		String id = idc.getParameter("id");
		String name = idc.getParameter("name");
		String osType = idc.getParameter("osType");
		String deviceTypeMap = idc.getParameter("img");//;
		String deviceTypeMapContent = idc.getParameter("image");

		if (StringUtil.isEmpty(pid)) {
			bean.fail("资产类型大类编号不能为空");
			return bean;
		}
		if (StringUtil.isEmpty(id)) {
			bean.fail("资产类型编号不能为空");
			return bean;
		}
		if (StringUtil.isEmpty(name)) {
			bean.fail("资产类型名称不能为空");
			return bean;
		}
		if (StringUtil.isEmpty(osType)) {
			vo.setOsType("108");
		} else {
			vo.setOsType(osType);
		}

		vo.setDeviceClass(Long.parseLong(pid));
		vo.setDeviceType(id);
		vo.setDeviceTypeName(name);
		vo.setOsType(osType);
		vo.setDeviceTypeMap(deviceTypeMap);
		vo.setDeviceTypeMapContent(deviceTypeMapContent);


		// vo.setDeviceType(request.getParameter("deviceType"));
		// vo.setDeviceTypeName(request.getParameter("deviceTypeName"));
		// vo.setOlddeviceTypeName(request.getParameter("olddeviceTypeName"));
		// vo.setDeviceClass(Long.parseLong(request.getParameter("deviceClass")));
		// vo.setOsType(request.getParameter("osType"));

		try {
			int rtn = equipmentTypeService.updEquipmentTypeSave(request,vo);
			switch (rtn) {
			case -1:
				bean.fail("修改资产类型同步失败");
				break;
			case 1:
				bean.ok("修改资产类型同步成功");
				break;
			case 2:
				bean.fail("该资产类型名已经存在");
				break;
			case 3:
				bean.fail("改资产类型编号不存在");
				break;
			case 9:
				bean.fail("该资产类型大类编号不存在");
				break;
			}
		} catch (AppException e) {
			bean.fail("修改设备类型失败");
			logger.error("修改设备类型失败", e);
		}
		return bean;
	}

	/**
	 * 批量删除资产类型信息
	 * 
	 * @param param
	 * @param session
	 * @param request
	 * @return
	 * @throws Exception
	 */
	@ResponseBody
	@RequestMapping("delEquipmentType")
	public AppResultBean delBatch(@RequestParam("param") String param, HttpSession session, HttpServletRequest request)
			throws Exception {
		logger.info("param=[" + param + "]");
		AppResultBean bean = new AppResultBean();
		IDataCenter idc = BeanUtils.toDataCenter(param);
		EquipmentTypeVO vo = (EquipmentTypeVO) BeanUtils.jsonToBean(idc, EquipmentTypeVO.class);
		List<EquipmentTypeVO> list = new ArrayList<EquipmentTypeVO>();

		String id = idc.getParameter("deviceIds");
		vo.setDeviceType(id);
		String str = vo.getDeviceType();
		String[] strs = str.split(",");
		for (int i = 0; i < strs.length; i++) {
			EquipmentTypeVO etvo = new EquipmentTypeVO();
			etvo.setDeviceType(strs[i]);
			list.add(etvo);
		}
		try {
			List<EquipmentTypeVO> retlist = equipmentTypeService.preBatchDel(list);
			if (!retlist.isEmpty()) {
				bean.fail("资产类型已经被资产引用，无法删除");
			} else {
				equipmentTypeService.delBatch(list, request);
				bean.ok("资产类型删除同步成功。");
			}
		} catch (AppException e) {
			logger.error("资产类型删除同步失败", e);
			bean.fail("资产类型删除同步失败。");
		}
		return bean;
	}

	/**
	 * 批量删除前查询是否可以删除
	 * 
	 * @param param
	 * @param session
	 * @return
	 * @throws Exception
	 */
	@ResponseBody
	@RequestMapping("preETBatchDel")
	public AppResultBean preBatchDel(@RequestParam("param") String param, HttpSession session) throws Exception {
		AppResultBean bean = new AppResultBean();
		IDataCenter idc = BeanUtils.toDataCenter(param);
		EquipmentTypeVO vo = (EquipmentTypeVO) BeanUtils.jsonToBean(idc, EquipmentTypeVO.class);
		List<EquipmentTypeVO> list = new ArrayList<EquipmentTypeVO>();
		String str = vo.getDeviceType();
		String[] strs = str.split(",");
		String names = "";
		for (int i = 0; i < strs.length; i++) {
			EquipmentTypeVO etvo = new EquipmentTypeVO();
			String[] temp = strs[i].split("_");
			if (temp.length == 2) {
				etvo.setDeviceType(temp[0]);
				list.add(etvo);
				names = names + temp[1] + ",";
			}
		}

		if (StringUtils.isNotEmpty(names)) {
			names = names.substring(0, names.length() - 1);
		}
		try {
			List<EquipmentTypeVO> retlist = equipmentTypeService.preBatchDel(list);

			if (retlist.isEmpty()) {
				bean.ok("可以删除。");
			} else {
				String message = "";
				for (EquipmentTypeVO voi : retlist) {
					message += voi.getDeviceTypeName() + ",";
				}
				message = message.substring(0, message.length() - 1);
				bean.fail("不可以删除。");
			}

		} catch (AppException e) {
			logger.error("删除类型失败", e);
			bean.fail("删除失败。");
		}
		return bean;
	}

}
