package com.rh.soc.equipmenttypethirdinterface.dao;

import java.util.List;

import org.springframework.jdbc.support.rowset.SqlRowSet;

import com.rh.soc.management.basedata.equipmenttype.model.EquipmentClassVO;
import com.rh.soc.management.basedata.equipmenttype.model.EquipmentTypeVO;

public interface IEquipmentTypeThirdDAO
{
    /**
     * 页面初始，查询所有数据；修改时根据id查询单一数据
     * 
     */
    public List<EquipmentTypeVO> queryEquipmentTypeData(EquipmentTypeVO vo);
    
    
	public List<EquipmentTypeVO> queryEquipmentTypeDataUpdate(EquipmentTypeVO vo) ;

    /**
     * 查询总类型数据
     * 
     */
    public List<EquipmentClassVO> queryClassType(EquipmentClassVO vo);
    
    /**
     * 
     * {查询操作系统列表}
     * 
     */
    public List<EquipmentClassVO> queryOsType(EquipmentClassVO vo);

    /**
     * 添加页面保存数据
     * 
     */
    public int addEquipmentTypeSave(EquipmentTypeVO vo);

    /**
     * 保存修改数据
     * 
     */
    public int updEquipmentTypeSave(EquipmentTypeVO vo);

    /**
     * 删除选择数据
     * 
     */
    public int delEquipmentType(EquipmentTypeVO vo);

    /**
     * 查询某种设备类型是否在已有实体应用
     * 
     */
    public SqlRowSet queryDataFromEntityDevice(long deviceClass);

    /**
     * 
     * 将资产的类型由已知类型改为未知
     * 
     */
	public int updAssetType(EquipmentTypeVO voi);
	
	 /**
	 * 
	 * {删除资产类型关联的操作系统}
	 * 
	 */
	public int delAssetTypeVsOs(String deviceType);
	
	/**
	 * 
	 * {添加资产类型关联的操作系统}
	 * 
	 */
	public int addAssetTypeVsOs(String deviceType, String osType);
	
}
