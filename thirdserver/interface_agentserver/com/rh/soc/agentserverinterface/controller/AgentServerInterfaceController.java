package com.rh.soc.agentserverinterface.controller;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.rh.soc.management.systemconfig.appconfig.model.AppServerInfoVO;
import com.rh.soc.management.systemconfig.appconfig.service.IAppConfigService;
import com.rh.webserver.common.base.bean.AppResultBean;
import com.rh.webserver.common.base.controller.BaseController;
import com.rh.webserver.common.base.json.IDataCenter;
import com.rh.webserver.common.util.BeanUtils;

@Controller
@RequestMapping("third/appconfig")
public class AgentServerInterfaceController extends BaseController {

	@Resource
	IAppConfigService SERVICE;

	/**
	 * {删除代理服务器信息}
	 */
	@ResponseBody
	@RequestMapping("delAgentServerInfo")
	public AppResultBean delAgentServerInfo(@RequestParam("param") String param, HttpSession session,
			HttpServletRequest request, HttpServletResponse response) throws Exception {
		AppResultBean bean = new AppResultBean();
		try {
			IDataCenter idc = BeanUtils.toDataCenter(param);
			String id = idc.getParameter("id");
			AppServerInfoVO vo = new AppServerInfoVO();
			vo.setPageSize(99999999);
			vo.setCurrentPageNum(1);
			vo.setNodeId(id);
			vo.setCanDeleteDomain(true);
			int rtn = SERVICE.delAgentServerInfo(vo);
			switch (rtn) {
			case -1:
				bean.fail("删除失败。");
				// idc.setCode(-1);
				// log.log(request,
				// ResourceFactory.getString("log.operation.delete"),
				// ResourceFactory.getString("log.module.management.systemconfig.proxyset"),
				// false);

				break;
			case 1:
				// refreshCodelist("log_agent_codelist", "log_agent_codelist");
				// log.log(request,
				// ResourceFactory.getString("log.operation.delete"),
				// ResourceFactory.getString("log.module.management.systemconfig.proxyset"),
				// true);
				break;
			case 2:
				bean.fail("该代理服务器被监控器使用，不允许删除。");
				break;
			case 3:
				bean.fail("该代理服务器被域使用，不允许删除。");
				break;
			}
		} catch (Exception e) {
			logger.error("删除代理服务器配置失败", e);
			// log.log(request,
			// ResourceFactory.getString("log.operation.delete"),
			// ResourceFactory.getString("log.module.management.systemconfig.appserverset"),
			// false);
			bean.fail("删除失败。");
		}
		return bean;
	}

}
