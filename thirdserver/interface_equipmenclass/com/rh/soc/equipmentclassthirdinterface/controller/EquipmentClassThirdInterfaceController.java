package com.rh.soc.equipmentclassthirdinterface.controller;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.rh.soc.equipmentclassthirdinterface.service.IEquipmentClassThirdService;
import com.rh.soc.management.basedata.equipmenttype.model.EquipmentClassVO;
import com.rh.webserver.common.base.bean.AppResultBean;
import com.rh.webserver.common.base.controller.BaseController;
import com.rh.webserver.common.base.exception.AppException;
import com.rh.webserver.common.base.json.IDataCenter;
import com.rh.webserver.common.util.BeanUtils;
import com.rh.webserver.common.util.StringUtil;

@Controller
@RequestMapping("/third/EquipmentClass")
public class EquipmentClassThirdInterfaceController extends BaseController {
	private static final Logger logger = Logger.getLogger(EquipmentClassThirdInterfaceController.class);

	@Resource
	IEquipmentClassThirdService equipmentTypeService;

	
	/**
	 * 添加资产类型信息
	 * 添加页面保存数据
	 * @param param
	 * @param request
	 * @param session
	 * @return
	 * @throws Exception
	 */
	@ResponseBody
	@RequestMapping("addEquipmentClass")
	public synchronized AppResultBean addEquipmentTypeSave(@RequestParam("param") String param, HttpSession session)
			throws Exception {
		logger.info("param=["+param+"]");
		AppResultBean bean = new AppResultBean();
		try {
			IDataCenter idc = BeanUtils.toDataCenter(param);
			EquipmentClassVO vo = BeanUtils.jsonToBean(idc, EquipmentClassVO.class);
			String id=idc.getParameter("id");
			String name=idc.getParameter("name");
			
			if(StringUtil.isEmpty(id)){
				bean.fail("资产类型编号不能为空");
				return bean;
			}
			if(StringUtil.isEmpty(name)){
				bean.fail("资产类型名称不能为空");
				return bean;
			}
			vo.setDeviceClass(Long.parseLong(id));
			vo.setDeviceClassName(name);
			int rtn = equipmentTypeService.addEquipmentTypeSave(vo);
			bean.fail("rtn"+rtn);
			switch (rtn) {
			case -1:
				bean.fail("添加失败");
				break;
			case 1:
				bean.ok("添加资产类型同步成功");
				break;
			case 2:
				bean.fail("该资产类型名称已经存在");
				break;
			case 3:
				bean.fail("该资产类型编号已经存在");
				break;
			}
		} catch (AppException e) {
			bean.fail("添加资产类型失败");
			logger.error("添加资产类型失败", e);
		}
		return bean;
	}

	/**
	 * 更新修改资产类型信息
	 * 保存修改数据
	 * @param param
	 * @param session
	 * @param request
	 * @return
	 * @throws Exception
	 */
	@ResponseBody
	@RequestMapping("updEquipmentClass")
	public AppResultBean updEquipmentTypeSave(@RequestParam("param") String param, HttpSession session)
			throws Exception {
		logger.info("param=["+param+"]");
		AppResultBean bean = new AppResultBean();
		try {
			IDataCenter idc = BeanUtils.toDataCenter(param);
			EquipmentClassVO vo = BeanUtils.jsonToBean(idc, EquipmentClassVO.class);
			String id=idc.getParameter("id");
			String name=idc.getParameter("name");
			
			if(StringUtil.isEmpty(id)){
				bean.fail("资产类型编号不能为空");
				return bean;
			}
			if(StringUtil.isEmpty(name)){
				bean.fail("资产类型名称不能为空");
				return bean;
			}
			
			vo.setDeviceClass(Long.parseLong(id));
			vo.setDeviceClassName(name);
			int rtn = equipmentTypeService.updEquipmentTypeSave(vo);
			switch (rtn) {
			case -1:
				bean.fail("修改失败");
				break;
			case 1:
				bean.ok("修改成功");
				break;
			case 2:
				bean.fail("该类型名已经存在");
				break;
			case 3:
				bean.fail("该资产类型编号不存在");
				break;
			case 4:
				bean.fail("该资产类型编号已经存在");
				break;
			}
		} catch (AppException e) {
			bean.fail("修改设备类型失败");
			logger.error("修改设备类型失败", e);
		}
		return bean;
	}

	/**
	 * 批量删除资产类型信息
	 * 
	 * @param param
	 * @param session
	 * @param request
	 * @return
	 * @throws Exception
	 */
	@ResponseBody
	@RequestMapping("delEquipmentClass")
	public AppResultBean delBatch(@RequestParam("param") String param, HttpSession session, HttpServletRequest request)
			throws Exception {
		logger.info("param=["+param+"]");
		AppResultBean bean = new AppResultBean();
		IDataCenter idc = BeanUtils.toDataCenter(param);
		EquipmentClassVO vo = BeanUtils.jsonToBean(idc, EquipmentClassVO.class);
		List<EquipmentClassVO> list = new ArrayList<EquipmentClassVO>();
		String id=idc.getParameter("id");
		if(StringUtil.isEmpty(id)){
			bean.fail("资产类型编号不能为空");
			return bean;
		}
		String name=idc.getParameter("name");
		vo.setDeviceClassStr(id);
//		vo.setDeviceClassoldStr(ids);
		String str = vo.getDeviceClassStr();
		String[] strs = str.split(",");
		for (int i = 0; i < strs.length; i++) {
			EquipmentClassVO etvo = new EquipmentClassVO();
			etvo.setDeviceClass(Long.parseLong(strs[i]));
			list.add(etvo);
		}
		try {
			List<EquipmentClassVO> retlist = equipmentTypeService.preBatchDel(list);
			if (!retlist.isEmpty()) {
				bean.fail("资产类型已经被资产引用，无法删除");
			} else {
				equipmentTypeService.delBatch(list);
				bean.ok("资产类型删除同步成功。");
			}
		} catch (AppException e) {
			logger.error("删除资产类型失败", e);
			bean.fail("删除资产类型失败。");
		}
		return bean;
	}
	

}
