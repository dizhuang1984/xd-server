package com.rh.soc.equipmentclassthirdinterface.service.impl;

import org.apache.log4j.Logger;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

@Component
public class AsyncTest {

	static Logger logger = Logger.getLogger(AsyncTest.class);

	@Async("executor")
	public void test() {
		logger.info("async called");
	}

	public void test2() {
		test();
	}

}
