package com.rh.soc.equipmentclassthirdinterface.service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.log4j.Logger;
import org.springframework.jdbc.support.rowset.SqlRowSet;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.rh.soc.equipmentclassthirdinterface.dao.IEquipmentClassThirdDAO;
import com.rh.soc.equipmentclassthirdinterface.service.IEquipmentClassThirdService;
import com.rh.soc.management.basedata.equipmenttype.model.EquipmentClassVO;
import com.rh.webserver.common.base.cache.CodeList;
import com.rh.webserver.common.base.exception.AppException;
import com.rh.webserver.common.base.service.BaseService;

/**
 * 
 */
@Service
@Transactional
public class EquipmentClassThirdServiceImpl extends BaseService implements IEquipmentClassThirdService {

	private static final Logger logger = Logger.getLogger(EquipmentClassThirdServiceImpl.class);

	@Resource
	private IEquipmentClassThirdDAO dao;

	/**
	 * 添加页面保存数据}
	 * 
	 */
	public int addEquipmentTypeSave(EquipmentClassVO vo) throws AppException {
		try {
			int rtn = 0;
			List<EquipmentClassVO> nameUsed = dao.queryEquipmentTypeData(vo);// 查询是否有重名类型

			List<EquipmentClassVO> idUsed = dao.queryEquipmentTypeDataById(vo);// 查询是否有重名类型

			if (idUsed.size() > 0) {
				rtn = 3;
			}
			else if (nameUsed.size() > 0) {
				rtn = 2;
			} 
			else{
				rtn = dao.addEquipmentTypeSaveThird(vo);
				refreshCodeList("equipmentClasslist");
			}
			logger.info("addEquipmentTypeSave service rtn="+rtn);
			return rtn;
		} catch (Exception e) {
			logger.error("插入设备类型失败。", e);
			throw new AppException(e);
		}
	}

//	/**
//	 * 页面初始，查询所有数据；修改时根据id查询单一数据
//	 * 
//	 */
//	public List<EquipmentClassVO> queryEquipmentTypeData(EquipmentClassVO vo) throws AppException {
//		try {
//			return dao.query(vo);
//		} catch (Exception e) {
//			logger.error("获取数据失败。", e);
//			throw new AppException(e);
//		}
//
//	}

	/**
	 * 查询总类型数据
	 */
	public Map<String, List<EquipmentClassVO>> queryClassType(EquipmentClassVO vo) throws AppException {
		try {
			Map<String, List<EquipmentClassVO>> map = new HashMap<String, List<EquipmentClassVO>>();
			List<EquipmentClassVO> deviceList = dao.queryClassType(vo);
			map.put("device", deviceList);
			return map;
		} catch (Exception e) {
			logger.error("获取数据失败。", e);
			throw new AppException(e);
		}

	}

	/**
	 * 保存修改数据
	 * 
	 */
	public int updEquipmentTypeSave(EquipmentClassVO vo) throws AppException {
		try {
			int rtn = 0;
			
			List<EquipmentClassVO> idUsed = dao.queryEquipmentTypeDataById(vo);// 查询是否有重名类型
			
			List<EquipmentClassVO> nameUsed = dao.queryEquipmentTypeDataForUpdate(vo);// 查询是否有重名类型

			
			if (idUsed.size() == 0) {
				rtn = 3;
			}
			
			else if (nameUsed.size() > 0) {
				rtn = 2;
			} 
			else{
				rtn = dao.updEquipmentTypeSave(vo);
				refreshCodeList("equipmentClasslist");
			}
			
			return rtn;
		} catch (

		Exception e) {
			logger.error("修改设备类型失败。", e);
			throw new AppException(e);
		}
	}

	/**
	 * 确认类型是否被使用
	 */
	private SqlRowSet queryDataFromEntityDevice(long deviceClass) throws AppException {
		SqlRowSet result = dao.queryDataFromEntityDevice(deviceClass);
		return result;
	}

	/**
	 * 批量删除
	 */
	@Override
	public int delBatch(List<EquipmentClassVO> list) throws AppException {
		try {
			for (EquipmentClassVO voi : list) {
				dao.delEquipmentType(voi);
			}
			return 1;
		} catch (Exception e) {
			logger.error("批量删除失败", e);
			throw new AppException(e);
		}
	}

	/**
	 * 
	 * 批量删除前查询是否可以删除
	 * 
	 */
	@Override
	public List<EquipmentClassVO> preBatchDel(List<EquipmentClassVO> list) throws AppException {
		try {
			List<EquipmentClassVO> retList = new ArrayList<EquipmentClassVO>();
			for (EquipmentClassVO voi : list) {
				if (queryDataFromEntityDevice(voi.getDeviceClass()).next()) {
					retList.add(voi);
				}
			}
			return retList;
		} catch (Exception e) {
			logger.error("批量删除前查询失败", e);
			throw new AppException(e);
		}
	}

	/**
	 * 
	 * {同步资产类型}
	 * 
	 */
	// public void syncEquipmentType() throws AppException {
	// try {
	// EquipmentClassVO classVo = new EquipmentClassVO();
	// classVo.setPageSize(9999999);
	// classVo.setCurrentPageNum(1);
	// List<EquipmentClassVO> deviceClassList =
	// dao.queryClassType(classVo);
	//
	// EquipmentTypeVO typeVo = new EquipmentTypeVO();
	// typeVo.setPageSize(9999999);
	// typeVo.setCurrentPageNum(1);
	// List<EquipmentTypeVO> deviceTypeList =
	// dao.queryEquipmentTypeData(typeVo);
	//
	// String result = EquipmentTypeXml(deviceClassList, deviceTypeList);
	// ITSMClient.doITSMService("asset_type_sync", result);
	// }catch (Exception e) {
	// logger.error("同步资产类型失败", e);
	// throw new AppException(e);
	// }
	// }

	/**
	 * 
	 * {创建xml结构}
	 * 
	 */
	// private String EquipmentTypeXml(List<EquipmentClassVO> deviceClassList,
	// List<EquipmentTypeVO> deviceTypeList) {
	// Document document = org.dom4j.DocumentHelper.createDocument();
	// document.setXMLEncoding("gbk");
	// Element root = document.addElement("root");
	// for (EquipmentClassVO vo : deviceClassList) {
	// Element deviceClass = root.addElement("deviceClass");
	// deviceClass.addElement("deviceClassId").setText(vo.getDeviceClass() == 0
	// ? "" : String.valueOf(vo.getDeviceClass()));// 资产类型大类ID
	// deviceClass.addElement("deviceClassName").setText(StringUtils.isEmpty(vo.getDeviceClassName())
	// ? "" : vo.getDeviceClassName());// 资产类型大类名称
	// deviceClass.addElement("deviceClassSort").setText(StringUtils.isEmpty(vo.getDeviceClassSort())
	// ? "" : vo.getDeviceClassSort());// 资产类型大类权重
	// }
	// for (EquipmentTypeVO vo : deviceTypeList) {
	// Element deviceType = root.addElement("deviceType");
	// deviceType.addElement("deviceTypeId").setText(vo.getDeviceType());//
	// 资产类型小类ID
	// deviceType.addElement("deviceClassId").setText(vo.getDeviceClass() == 0 ?
	// "" : String.valueOf(vo.getDeviceClass()));// 资产类型大类ID
	// deviceType.addElement("deviceTypeName").setText(StringUtils.isEmpty(vo.getDeviceTypeName())
	// ? "" : vo.getDeviceTypeName());// 资产类型小类名称
	// deviceType.addElement("deviceTypeSort").setText(vo.getDeviceTypeSort() ==
	// 0 ? "" : String.valueOf(vo.getDeviceTypeSort()));// 资产类型小类权重
	// }
	// return document.asXML();
	// }

	/**
	 * 查询资产类型（华能定制）
	 * 
	 */
	// public String queryAssetTypeForHuaneng() throws AppException
	// {
	// EquipmentTypeVO vo = new EquipmentTypeVO();
	// vo.setCurrentPageNum(1);
	// vo.setPageSize(10000);
	// List<EquipmentTypeVO> list =
	// dao.queryEquipmentTypeData(vo);
	// Document d = DocumentHelper.createDocument();
	// d.setXMLEncoding("gbk");
	// Element deviceTypeRoot = d.addElement("root");
	// Element equipmentTypes = deviceTypeRoot.addElement("equipmentTypes");
	// for (EquipmentTypeVO equipmentTypeVO : list) {
	// Element equipmentType = equipmentTypes.addElement("equipmentType");
	// equipmentType.addElement("deviceType").setText(equipmentTypeVO.getDeviceType()+"");
	// equipmentType.addElement("deviceClass").setText(equipmentTypeVO.getDeviceClass()+"");
	// equipmentType.addElement("deviceTypeName").setText(equipmentTypeVO.getDeviceTypeName()==null?"":equipmentTypeVO.getDeviceTypeName());
	// equipmentType.addElement("deviceClassName").setText(equipmentTypeVO.getDeviceClassName()==null?"":equipmentTypeVO.getDeviceClassName());
	// equipmentType.addElement("osName").setText(equipmentTypeVO.getOsName()==null?"":equipmentTypeVO.getOsName());
	// equipmentType.addElement("deviceTypeMap").setText(equipmentTypeVO.getDeviceTypeMap()==null?"":equipmentTypeVO.getDeviceTypeMap());
	// }
	// return d.asXML();
	//
	// }

	@CodeList("equipmentClasslist")
	@Override
	public List<EquipmentClassVO> queryEquipmentClass(EquipmentClassVO vo) throws AppException {
		try {
			List<EquipmentClassVO> deviceList = dao.queryClassType(vo);
			return deviceList;
		} catch (Exception e) {
			logger.error("获取数据失败。", e);
			throw new AppException(e);
		}
	}



}
