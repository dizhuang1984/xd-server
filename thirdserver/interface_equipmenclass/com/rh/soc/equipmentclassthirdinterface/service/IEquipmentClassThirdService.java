package com.rh.soc.equipmentclassthirdinterface.service;

import java.util.List;
import java.util.Map;

import com.rh.soc.management.basedata.equipmenttype.model.EquipmentClassVO;
import com.rh.webserver.common.base.exception.AppException;

public interface IEquipmentClassThirdService {
	/**
	 * 页面初始，查询所有数据；修改时根据id查询单一数据
	 * 
	 */
	/**
	 * 添加页面保存数据
	 * 
	 */
	public int addEquipmentTypeSave(EquipmentClassVO vo) throws AppException;

	/**
	 * 保存修改数据
	 * 
	 */
	public int updEquipmentTypeSave(EquipmentClassVO vo) throws AppException;

	/**
	 * 查询总类型数据
	 * 
	 */
	public Map<String, List<EquipmentClassVO>> queryClassType(EquipmentClassVO vo) throws AppException;

	/**
	 * 
	 * 批量删除前查询是否可以删除
	 * 
	 */
	public List<EquipmentClassVO> preBatchDel(List<EquipmentClassVO> list) throws AppException;

	/**
	 * 
	 * 批量删除
	 * 
	 */
	public int delBatch(List<EquipmentClassVO> list) throws AppException;

	/**
	 * 
	 * {同步资产类型}
	 * 
	 */
	// public void syncEquipmentType() throws AppException;

	/**
	 * 查询资产类型（华能定制）
	 * 
	 */
	public List<EquipmentClassVO> queryEquipmentClass(EquipmentClassVO vo) throws AppException;
	

}
