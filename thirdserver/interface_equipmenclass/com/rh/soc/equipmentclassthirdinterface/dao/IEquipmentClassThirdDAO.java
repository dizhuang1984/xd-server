package com.rh.soc.equipmentclassthirdinterface.dao;

import java.util.List;

import org.springframework.jdbc.support.rowset.SqlRowSet;

import com.rh.soc.management.basedata.equipmenttype.model.EquipmentClassVO;

public interface IEquipmentClassThirdDAO {
	
	/**
	 * 页面初始，查询所有数据；修改时根据id查询单一数据
	 * 
	 */
	public List<EquipmentClassVO> queryEquipmentTypeData(EquipmentClassVO vo);
	
	
	public List<EquipmentClassVO> queryEquipmentTypeDataById(EquipmentClassVO vo);
	
	
	public List<EquipmentClassVO> queryEquipmentTypeDataForUpdate(EquipmentClassVO vo);
	
	

	/**
	 * 查询总类型数据
	 * 
	 */
	public List<EquipmentClassVO> queryClassType(EquipmentClassVO vo);

	/**
	 * 添加页面保存数据
	 * 
	 */
	public int addEquipmentTypeSave(EquipmentClassVO vo);

	/**
	 * 保存修改数据
	 * 
	 */
	public int updEquipmentTypeSave(EquipmentClassVO vo);

	/**
	 * 删除选择数据
	 * 
	 */
	public int delEquipmentType(EquipmentClassVO vo);

	/**
	 * 查询某种设备类型是否在已有实体应用
	 * 
	 */
	public SqlRowSet queryDataFromEntityDevice(long deviceClass);
	
	
	public int addEquipmentTypeSaveThird(EquipmentClassVO vo);
	
	public int updEquipmentTypeSaveThird(EquipmentClassVO vo);

}
