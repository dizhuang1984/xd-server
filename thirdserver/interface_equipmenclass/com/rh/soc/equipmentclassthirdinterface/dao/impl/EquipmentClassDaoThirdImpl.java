package com.rh.soc.equipmentclassthirdinterface.dao.impl;

import java.util.List;

import org.springframework.jdbc.support.rowset.SqlRowSet;
import org.springframework.stereotype.Repository;

import com.rh.soc.equipmentclassthirdinterface.dao.IEquipmentClassThirdDAO;
import com.rh.soc.management.basedata.equipmenttype.model.EquipmentClassVO;
import com.rh.webserver.common.base.dao.BaseDao;

@Repository
public class EquipmentClassDaoThirdImpl extends BaseDao implements IEquipmentClassThirdDAO {

	/**
	 * 页面初始，查询所有数据；修改时根据id查询单一数据
	 */
	public List<EquipmentClassVO> queryEquipmentTypeData(EquipmentClassVO vo) {
		StringBuilder sql = new StringBuilder(128);
		sql.append(
				"select device_class,device_class_name,device_class_sort from t_device_class where device_class_name = ?");
		String str = this.packageSqlForOrderBy(sql.toString(), vo.getOrderBy(), vo.getOrder());
		return this.query(str, vo, vo.getDeviceClassName());
	}
	
	public List<EquipmentClassVO> queryEquipmentTypeDataById(EquipmentClassVO vo) {
		StringBuilder sql = new StringBuilder(128);
		sql.append(
				"select device_class,device_class_name,device_class_sort from t_device_class where device_class = ?");
		String str = this.packageSqlForOrderBy(sql.toString(), vo.getOrderBy(), vo.getOrder());
		return this.query(str, vo, vo.getDeviceClass());
	}
	
	/**
	 * 页面初始，查询所有数据；修改时根据id查询单一数据
	 */
	public List<EquipmentClassVO> queryEquipmentTypeDataForUpdate(EquipmentClassVO vo) {
		StringBuilder sql = new StringBuilder(128);
		sql.append(
				"select device_class,device_class_name,device_class_sort from t_device_class where device_class_name = '"+vo.getDeviceClassName()+"' and device_class!="+vo.getDeviceClass());
		String str = this.packageSqlForOrderBy(sql.toString(), vo.getOrderBy(), vo.getOrder());
		return this.query(str, vo);
	}
	

	/**
	 * 查询总类型数据
	 * 
	 */
	public List<EquipmentClassVO> queryClassType(EquipmentClassVO vo) {
		String sql = "select * from ( select B.device_class as device_class, " + " B.device_class_name || nvl(A.c,'(0)') as device_class_name, "
				+ " B.device_class_sort as device_class_sort " + " from t_device_class B left join  "
				+ " (SELECT t1.device_class, '(' || count(1) || ')' as c "
				+ " FROM t_device_class t1, t_device_type t2 " + " where t1.device_class = t2.device_class "
				+ " group by t1.device_class) A " + " on B.device_class = A.device_class) where device_class != 5 "
				+ " order by device_class_sort";
		return this.query(sql, vo);
	}


	/**
	 * 添加页面保存数据
	 * 
	 */
	public int addEquipmentTypeSave(EquipmentClassVO vo) {
		String sql2 = "SELECT max(device_class_sort) FROM t_device_class ORDER BY device_class_sort DESC";
		long max = this.queryForLong(sql2) + 1;
		String deviceClass = nextVal("seq_device_class");
		String sql = "INSERT INTO t_device_class  (device_class ,device_class_name,DEVICE_CLASS_MAP,"
				+ "device_class_sort) VALUES (?,?,'/',?)";
		Object[] o = new Object[] { Long.parseLong(deviceClass), vo.getDeviceClassName(), max };
		return this.update(sql, o);
	}

	/**
	 * 保存修改数据
	 * 
	 */
	public int updEquipmentTypeSave(EquipmentClassVO vo) {
		String sql = "UPDATE t_device_class SET device_class_name = ? " + " WHERE device_class = ?";
		Object[] o = new Object[] { vo.getDeviceClassName(), vo.getDeviceClass() };
		return this.update(sql, o);
	}

	/**
	 * 删除选择数据
	 * 
	 */
	public int delEquipmentType(EquipmentClassVO vo) {
		String sql = "DELETE FROM t_device_class WHERE device_class = ?";
		Object[] o = new Object[] { vo.getDeviceClass() };
		return this.update(sql, o);
	}

	/**
	 * 查询实体设备表，确认设备类型是否被使用
	 * 
	 */
	public SqlRowSet queryDataFromEntityDevice(long deviceType) {
		String sql = "select * from t_device_type where device_class = " + deviceType;
		return this.query(sql);
	}
	
	
	public int addEquipmentTypeSaveThird(EquipmentClassVO vo) {
		long sortLong=0;
		String sql2;
		try {
			sql2 = "SELECT max(device_class_sort) FROM t_device_class ORDER BY device_class_sort DESC";
			sortLong = this.queryForLong(sql2);
		} catch (Exception e) {
			sortLong=0;
		}
		long max = sortLong + 1;
		String sql = "INSERT INTO t_device_class  (device_class ,device_class_name,DEVICE_CLASS_MAP,"
				+ "device_class_sort) VALUES (?,?,'/',?)";
		Object[] o = new Object[] { vo.getDeviceClass(), vo.getDeviceClassName(), max };
		return this.update(sql, o);
	}

	/**
	 * 保存修改数据
	 * 
	 */
	public int updEquipmentTypeSaveThird(EquipmentClassVO vo) {
		String sql = "UPDATE t_device_class SET device_class_name = ? " + " WHERE device_class = ?";
		Object[] o = new Object[] { vo.getDeviceClassName(), vo.getDeviceClass() };
		return this.update(sql, o);
	}

}
