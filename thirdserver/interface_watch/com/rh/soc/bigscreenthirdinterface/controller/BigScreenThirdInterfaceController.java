package com.rh.soc.bigscreenthirdinterface.controller;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.rh.soc.monitor.config.model.InterfaceVO;
import com.rh.soc.monitor.config.service.IMonitorService;
import com.rh.soc.watch.bigscreen.model.BigScreenFaultAlarmEventVO;
import com.rh.soc.watch.bigscreen.model.BigScreenFaultMonitorVO;
import com.rh.soc.watch.bigscreen.model.BigScreenHostCpuMemDiskVO;
import com.rh.soc.watch.bigscreen.model.BigScreenHostCpuMemVO;
import com.rh.soc.watch.bigscreen.model.BigScreenHostDiskVO;
import com.rh.soc.watch.bigscreen.model.BigScreenMonitorClassInfoVO;
import com.rh.soc.watch.bigscreen.model.BigScreenMonitorInfoVO;
import com.rh.soc.watch.bigscreen.model.BigScreenNetFlowVO;
import com.rh.soc.watch.bigscreen.service.IBigScreenService;
import com.rh.webserver.common.base.bean.AppResultBean;
import com.rh.webserver.common.base.controller.BaseController;
import com.rh.webserver.common.base.exception.AppException;
import com.rh.webserver.common.base.json.IDataCenter;
import com.rh.webserver.common.util.BeanUtils;

@Controller
@RequestMapping("/third/bigscreen")
public class BigScreenThirdInterfaceController extends BaseController {

	public final static String SUCCESS = "success";

	@Resource
	IBigScreenService bigScreenService;

	@Resource
	IMonitorService monitorService;

	@RequestMapping("getMonitorFaultEventByParam")
	@ResponseBody
	public AppResultBean getMonitorFaultEventByParam(@RequestParam(value = "param", required = false) String param,
			HttpSession session) throws IOException {
		AppResultBean bean = new AppResultBean();
		try {
			IDataCenter idc = BeanUtils.toDataCenter(param);
			String userId = getUserId(session);
			BigScreenFaultAlarmEventVO vo = BeanUtils.jsonToBean(param, BigScreenFaultAlarmEventVO.class);
			vo.setUserId(userId);
			String monitorClass = idc.getParameter("monitorClass");
			List<BigScreenFaultAlarmEventVO> result = bigScreenService.getMonitorFaultEventByParam(monitorClass, vo);
			bean.ok(result);
		} catch (AppException e) {
			logger.error(e.getMessage(), e);
			bean.fail("查询数据异常");
		}
		return bean;
	}

	@RequestMapping("getMonitorCpuMemDiskInfoForAll")
	@ResponseBody
	public AppResultBean getMonitorCpuMemDiskInfoForAll(@RequestParam(value = "param", required = false) String param,
			HttpSession session) {
		AppResultBean bean = new AppResultBean();
		try {
			BigScreenHostCpuMemDiskVO vo = BeanUtils.jsonToBean(param, BigScreenHostCpuMemDiskVO.class);			
			String userId = getUserId(session);
			vo.setUserId(userId);
			vo.setOrderBy(" cpu_usage DESC, MEM_USAGE DESC,DISK_USAGE DESC,monitor_dstatus desc ,ed_name asc");		
			List<BigScreenHostCpuMemDiskVO> result = bigScreenService.getMonitorCpuMemDiskInfoForAll(vo);
			bean.ok(result);
		} catch (Exception e) {
			e.printStackTrace();
			bean.fail("异常");
		}
		return bean;
	}

	@RequestMapping("getMonitorCpuMemInfoForAll")
	@ResponseBody
	public AppResultBean getMonitorCpuMemInfoForAll(@RequestParam(value = "param", required = false) String param,
			HttpSession session) {
		AppResultBean bean = new AppResultBean();
		try {
			BigScreenHostCpuMemVO vo = BeanUtils.jsonToBean(param,BigScreenHostCpuMemVO.class);
			String userId = getUserId(session);
			vo.setUserId(userId);			
			vo.setOrderBy(" cpu_usage DESC, MEM_USAGE DESC,monitor_dstatus desc ,ed_name asc");		
			List<BigScreenHostCpuMemVO> result = bigScreenService.getMonitorCpuMemInfoForAll(vo);
			bean.ok(result);
		} catch (Exception e) {
			e.printStackTrace();
			bean.fail("异常");
		}
		return bean;
	}

	@RequestMapping("getMonitorCpuInfoForAll")
	@ResponseBody
	public AppResultBean getMonitorCpuInfoForAll(@RequestParam(value = "param", required = false) String param,
			HttpSession session) {
		AppResultBean bean = new AppResultBean();
		try {
			BigScreenHostCpuMemVO vo = BeanUtils.jsonToBean(param, BigScreenHostCpuMemVO.class);
			String userId = getUserId(session);
			vo.setUserId(userId);			
			vo.setOrderBy(" cpu_usage DESC, MEM_USAGE DESC,monitor_dstatus desc ,ed_name asc");		
			List<BigScreenHostCpuMemVO> result = bigScreenService.getMonitorCpuMemInfoForAll(vo);
			bean.ok(result);
		} catch (Exception e) {
			e.printStackTrace();
			bean.fail("异常");
		}
		return bean;
	}

	@RequestMapping("getMonitorMemInfoForAll")
	@ResponseBody
	public AppResultBean getMonitorMemInfoForAll(@RequestParam(value = "param", required = false) String param,
			HttpSession session) {
		AppResultBean bean = new AppResultBean();
		try {
			BigScreenHostCpuMemVO vo = BeanUtils.jsonToBean(param,BigScreenHostCpuMemVO.class);
			String userId = getUserId(session);
			vo.setUserId(userId);		
			vo.setOrderBy(" MEM_USAGE DESC, cpu_usage DESC  ,monitor_dstatus desc ,ed_name asc");		
			List<BigScreenHostCpuMemVO> result = bigScreenService.getMonitorCpuMemInfoForAll(vo);
			bean.ok(result);
		} catch (Exception e) {
			e.printStackTrace();
			bean.fail("异常");
		}
		return bean;
	}

	@RequestMapping("getPcInfoOrderbyCPU")
	@ResponseBody
	public AppResultBean getPcInfoOrderbyCPU(@RequestParam(value = "param", required = false) String param,
			HttpSession session) {
		AppResultBean bean = new AppResultBean();
		try {
			BigScreenHostCpuMemVO vo = BeanUtils.jsonToBean(param,BigScreenHostCpuMemVO.class);
			String userId = getUserId(session);
			vo.setUserId(userId);			
			vo.setOrderBy(" cpu_usage DESC, MEM_USAGE DESC,monitor_dstatus desc ,ed_name asc");		
			List<BigScreenHostCpuMemVO> result = bigScreenService.getPcInfo(vo);
			bean.ok(result);
		} catch (Exception e) {
			e.printStackTrace();
			bean.fail("异常");
		}
		return bean;
	}

	@RequestMapping("getPcInfoOrderbyMemory")
	@ResponseBody
	public AppResultBean getPcInfoOrderbyMemory(@RequestParam(value = "param", required = false) String param,
			HttpSession session) {
		AppResultBean bean = new AppResultBean();
		try {
			BigScreenHostCpuMemVO vo = BeanUtils.jsonToBean(param,BigScreenHostCpuMemVO.class);
			String userId = getUserId(session);
			vo.setUserId(userId);		
			vo.setOrderBy("MEM_USAGE DESC, cpu_usage DESC  ,monitor_dstatus desc ,ed_name asc");
			List<BigScreenHostCpuMemVO> result = bigScreenService.getPcInfo(vo);
			bean.ok(result);
		} catch (Exception e) {
			e.printStackTrace();
			bean.fail("异常");
		}
		return bean;
	}

	@RequestMapping("getMonitorDiskInfo")
	@ResponseBody
	public AppResultBean getPcInfoOrderbyDisk(@RequestParam(value = "param", required = false) String param,
			HttpSession session) {
		AppResultBean bean = new AppResultBean();
		try {
			BigScreenHostDiskVO vo = BeanUtils.jsonToBean(param,BigScreenHostDiskVO.class);
			String userId = getUserId(session);
			vo.setUserId(userId);
			vo.setOrderBy(" disk_usage DESC ,monitor_dstatus desc ,MOUNT_NAME asc,ed_name asc");
			List<BigScreenHostDiskVO> result = bigScreenService.getPcInfoDisk(vo);
			bean.ok(result);
		} catch (Exception e) {
			e.printStackTrace();
			bean.fail("异常");
		}
		return bean;
	}

	@RequestMapping("getNetInfoOrderbyCPUForZTE")
	@ResponseBody
	public AppResultBean getNetInfoOrderbyCPUForZTE(@RequestParam(value = "param", required = false) String param,
			HttpSession session) {
		AppResultBean bean = new AppResultBean();
		try {
			BigScreenHostCpuMemVO vo = new BigScreenHostCpuMemVO();
			String userId = getUserId(session);
			vo.setUserId(userId);
			vo.setCurrentPageNum(1);
			vo.setOrderBy(" cpu_usage DESC, MEM_USAGE DESC ,monitor_dstatus desc ,monitor_name asc");
			vo.setPageSize(1000);
			List<BigScreenHostCpuMemVO> result = bigScreenService.getNetCpuMemForZTE(vo);
			bean.ok(result);
		} catch (AppException e) {
			e.printStackTrace();
			bean.fail("异常");
		}
		return bean;
	}

	@RequestMapping("getNetInfoOrderbyMemoryForZTE")
	@ResponseBody
	public AppResultBean getNetInfoOrderbyMemoryForZTE(@RequestParam(value = "param", required = false) String param,
			HttpSession session) {
		AppResultBean bean = new AppResultBean();
		try {
			BigScreenHostCpuMemVO vo = new BigScreenHostCpuMemVO();
			String userId = getUserId(session);
			vo.setUserId(userId);
			vo.setCurrentPageNum(1);
			vo.setPageSize(1000);
			vo.setOrderBy(" MEM_USAGE DESC, cpu_usage DESC, monitor_dstatus desc ,monitor_name asc");
			List<BigScreenHostCpuMemVO> result = bigScreenService.getNetCpuMemForZTE(vo);
			bean.ok(result);
		} catch (AppException e) {
			e.printStackTrace();
			bean.fail("异常");
		}
		return bean;
	}

	@RequestMapping("getNetInfoOrderbyFlow")
	@ResponseBody
	public AppResultBean getNetInfoOrderbyFlow(@RequestParam(value = "param", required = false) String param,
			HttpSession session) {
		AppResultBean bean = new AppResultBean();
		try {
			BigScreenNetFlowVO vo = new BigScreenNetFlowVO();
			String userId = getUserId(session);
			vo.setUserId(userId);
			vo.setCurrentPageNum(1);
			vo.setPageSize(1000);
			vo.setOrderBy(" fault DESC, monitor_dstatus desc ,monitor_name asc");
			List<BigScreenNetFlowVO> result = bigScreenService.getNetFlowInfoForZTE(vo);
			bean.ok(result);
		} catch (AppException e) {
			logger.error(e.getMessage(), e);
			bean.fail("查询数据异常");
		}
		return bean;
	}

	@RequestMapping("getMonitorClassDstatus")
	@ResponseBody
	public AppResultBean getMonitorClassDstatus(@RequestParam("param") String param, HttpSession session)
			throws IOException {
		AppResultBean bean = new AppResultBean();
		String userId = getUserId(session);
		try {
			BigScreenMonitorClassInfoVO vo = new BigScreenMonitorClassInfoVO();
			vo.setUserId(userId);
			vo.setPageSize(1000);
			List<BigScreenMonitorClassInfoVO> result = bigScreenService.getMonitorClassDstatus(vo);
			bean.ok(result);
		} catch (AppException e) {
			e.printStackTrace();
			bean.fail("异常");
		}
		return bean;
	}

	@RequestMapping("getNetInfoOrderbyCPU")
	@ResponseBody
	public AppResultBean getNetInfoOrderbyCPU(@RequestParam(value = "param", required = false) String param,
			HttpSession session) {
		AppResultBean bean = new AppResultBean();
		try {
			BigScreenHostCpuMemVO vo = new BigScreenHostCpuMemVO();
			String userId = getUserId(session);
			vo.setUserId(userId);
			vo.setCurrentPageNum(1);
			vo.setPageSize(10);
			vo.setOrderBy(" cpu_usage DESC, MEM_USAGE DESC ,monitor_dstatus desc ,monitor_name asc");
			vo.setPageSize(10);
			List<BigScreenHostCpuMemVO> result = bigScreenService.getNetCpuMem(vo);
			bean.ok(result);
		} catch (AppException e) {
			e.printStackTrace();
			bean.fail("异常");
		}
		return bean;
	}

	@RequestMapping("getNetInfoOrderbyMemory")
	@ResponseBody
	public AppResultBean getNetInfoOrderbyMemory(@RequestParam(value = "param", required = false) String param,
			HttpSession session) {
		AppResultBean bean = new AppResultBean();
		try {
			BigScreenHostCpuMemVO vo = new BigScreenHostCpuMemVO();
			String userId = getUserId(session);
			vo.setUserId(userId);
			vo.setCurrentPageNum(1);
			vo.setPageSize(10);
			vo.setOrderBy(" MEM_USAGE DESC, cpu_usage DESC, monitor_dstatus desc ,monitor_name asc");
			List<BigScreenHostCpuMemVO> result = bigScreenService.getNetCpuMem(vo);
			bean.ok(result);
		} catch (AppException e) {
			e.printStackTrace();
			bean.fail("异常");
		}
		return bean;
	}

	@RequestMapping("getDataCenterMonitorFaultEvent")
	@ResponseBody
	public AppResultBean getDataCenterMonitorFaultEvent(@RequestParam(value = "param", required = false) String param,
			HttpSession session) throws IOException {
		AppResultBean bean = new AppResultBean();
		try {
			String userId = getUserId(session);
			BigScreenFaultAlarmEventVO vo = BeanUtils.jsonToBean(param, BigScreenFaultAlarmEventVO.class);
			vo.setUserId(userId);
			List<BigScreenFaultAlarmEventVO> result = bigScreenService.getDataCenterMonitorFaultEvent(vo);
			bean.ok(result);
		} catch (AppException e) {
			logger.error(e.getMessage(), e);
			bean.fail("查询数据异常");
		}
		return bean;
	}

	@RequestMapping("getNetMonitorFaultEvent")
	@ResponseBody
	public AppResultBean getNetMonitorFaultEvent(@RequestParam("param") String param, HttpSession session)
			throws IOException {
		AppResultBean bean = new AppResultBean();
		String userId = getUserId(session);
		try {
			/**
			 * 大屏查询故障事件
			 */
			BigScreenFaultAlarmEventVO vo = new BigScreenFaultAlarmEventVO();
			vo.setUserId(userId);
			List<BigScreenFaultAlarmEventVO> result = bigScreenService.getNetMonitorFaultEvent(vo);
			bean.ok(result);
		} catch (AppException e) {
			e.printStackTrace();
			bean.fail("异常");
		}
		return bean;
	}

	@RequestMapping("getBigScreenFaultMonitor")
	@ResponseBody
	public AppResultBean getBigScreenFaultMonitor(@RequestParam("param") String param, HttpSession session)
			throws IOException {
		AppResultBean bean = new AppResultBean();
		String userId = getUserId(session);
		try {
			BigScreenFaultMonitorVO vo = new BigScreenFaultMonitorVO();
			vo.setUserId(userId);
			vo.setPageSize(1000);
			List<BigScreenFaultMonitorVO> result = bigScreenService.getBigScreenFaultMonitor(vo);
			bean.ok(result);
		} catch (AppException e) {
			e.printStackTrace();
			bean.fail("异常");
		}
		return bean;
	}

	@RequestMapping("getMonitorClassDstatusTotalCnt")
	@ResponseBody
	public AppResultBean getMonitorClassDstatusTotalCnt(@RequestParam(value = "param", required = false) String param,
			HttpSession session) throws IOException {
		AppResultBean bean = new AppResultBean();
		try {
			InterfaceVO vo = new InterfaceVO();
			vo.setStatus(0);
			String ac = monitorService.queryActiveMonitorNumByParam(vo);
			vo.setStatus(1);
			String nac = monitorService.queryNotActiveMonitorNum(vo);
			bean.put("active", Integer.parseInt(ac));
			bean.put("notactive", Integer.parseInt(nac));
		} catch (AppException e) {
			logger.error(e.getMessage(), e);
			bean.fail("异常");
		}
		return bean;
	}

	@RequestMapping("getMonitorCpuMemDiskInfoForHost")
	@ResponseBody
	public AppResultBean getMonitorCpuMemDiskInfoForHost(@RequestParam(value = "param", required = false) String param,
			HttpSession session) {
		AppResultBean bean = new AppResultBean();
		try {
			BigScreenHostCpuMemDiskVO vo = new BigScreenHostCpuMemDiskVO();
			String userId = getUserId(session);
			vo.setUserId(userId);
			vo.setCurrentPageNum(1);
			vo.setPageSize(10);
			vo.setOrderBy(" cpu_usage DESC, MEM_USAGE DESC,DISK_USAGE DESC,monitor_dstatus desc ,ed_name asc");
			vo.setPageSize(99999);
			List<BigScreenHostCpuMemDiskVO> result = bigScreenService.getMonitorCpuMemDiskInfoForHost(vo);
			bean.ok(result);
		} catch (AppException e) {
			e.printStackTrace();
			bean.fail("异常");
		}
		return bean;
	}

	@RequestMapping("getMonitorCpuMemDiskInfoForHostSummary")
	@ResponseBody
	public AppResultBean getMonitorCpuMemDiskInfoForHostSummary(
			@RequestParam(value = "param", required = false) String param, HttpSession session) {
		AppResultBean bean = new AppResultBean();
		try {
			BigScreenHostCpuMemDiskVO vo = new BigScreenHostCpuMemDiskVO();
			String userId = getUserId(session);
			vo.setUserId(userId);
			vo.setCurrentPageNum(1);
			vo.setPageSize(99999);
			List<BigScreenMonitorInfoVO> result = bigScreenService.getMonitorCpuMemDiskInfoForHostSummary(vo);
			int good = 0;
			int nodata = 0;
			int perf = 0;
			int fault = 0;
			int auth = 0;
			Map<String, Object> m = new HashMap<>();
			if (result != null) {
				for (BigScreenMonitorInfoVO vox : result) {
					Integer mm = (Integer) m.get(vox.getDomaName());
					if (mm == null) {
						m.put(vox.getDomaName(), 1);
					} else {
						m.put(vox.getDomaName(), mm + 1);
					}
					int n = vox.getStatus();
					if (n == 0) {
						good++;
					} else if (n == 1) {
						nodata++;
					} else if (n == 2) {
						good++;
					} else if (n == 3) {
						perf++;
					} else {
						fault++;
					}
				}
			}
			bean.put("good", good);
			bean.put("nodata", nodata);
			bean.put("perf", perf);
			bean.put("fault", fault);
			bean.put("auth", auth);
			bean.put("total", good + nodata + perf + fault + auth);
			bean.put("domains",m);
		} catch (AppException e) {
			e.printStackTrace();
			bean.fail("异常");
		}
		return bean;
	}

}