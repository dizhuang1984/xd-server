package com.rh.soc.assetmonitor.controller;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.rh.soc.assetmanage.service.IAssetService;
import com.rh.soc.monitor.monitorview.os.model.MonitorCpuVo;
import com.rh.soc.monitor.monitorview.os.model.MonitorNetCardVo;
import com.rh.soc.monitor.monitorview.os.windows.model.HostFileVO;
import com.rh.soc.monitor.monitorview.os.windows.model.HostPortVO;
import com.rh.soc.monitor.monitorview.os.windows.model.ProcessVO;
import com.rh.soc.watch.assetmonitor.model.AssetMonitorCss;
import com.rh.soc.watch.assetmonitor.model.AssetMonitorTreeVO;
import com.rh.soc.watch.assetmonitor.model.AssetMonitorType;
import com.rh.soc.watch.assetmonitor.model.AssetMonitorVO;
import com.rh.soc.watch.assetmonitor.model.HardwareVo;
import com.rh.soc.watch.assetmonitor.model.MonitorBaselineVO;
import com.rh.soc.watch.assetmonitor.model.MonitorDiskVo;
import com.rh.soc.watch.assetmonitor.model.MonitorLeakVo;
import com.rh.soc.watch.assetmonitor.model.MonitorMainBoardVo;
import com.rh.soc.watch.assetmonitor.model.MonitorMemoryVo;
import com.rh.soc.watch.assetmonitor.model.MonitorVo;
import com.rh.soc.watch.assetmonitor.service.IAssetMonitorService;
import com.rh.soc.watch.bigscreen.model.BigScreenHostCpuMemVO;
import com.rh.soc.watch.bigscreen.service.IBigScreenService;
import com.rh.webserver.common.base.bean.AppResultBean;
import com.rh.webserver.common.base.bean.TreeVO;
import com.rh.webserver.common.base.controller.BaseController;
import com.rh.webserver.common.base.exception.AppException;
import com.rh.webserver.common.base.json.IDataCenter;
import com.rh.webserver.common.util.BeanUtils;

@Controller
@RequestMapping("/third/assetMonitor")
public class AssetMonitorControllerNew extends BaseController {

	private Logger logger = Logger.getLogger(error);

	@Resource
	private IAssetMonitorService assetMonitorService;

	@Resource
	private IAssetService assetService;

	@Resource
	private IBigScreenService bigScreen;

	/**
	 * 
	 * 查询基线检查事件
	 * 
	 */
	@ResponseBody
	@RequestMapping("queryBaseLineEvent")
	public AppResultBean queryBaseLineEvent(@RequestParam("param") String param, HttpSession session) throws Exception {
		IDataCenter idc = BeanUtils.toDataCenter(param);
		AppResultBean bean = new AppResultBean();
		MonitorBaselineVO vo = BeanUtils.jsonToBean(idc, MonitorBaselineVO.class);
		try {
			List<MonitorBaselineVO> list = assetMonitorService.queryBaseLineEvent(vo);
			bean.ok(list);
		} catch (Exception e) {
			logger.error("查询监控器异常", e);
			bean.fail("查询监控器异常");
		}
		return bean;
	}

	/**
	 * 
	 * 查询主板信息
	 * 
	 */
	@ResponseBody
	@RequestMapping("queryMainBoardInfo")
	public AppResultBean queryMainBoardInfo(@RequestParam("param") String param, HttpSession session) throws Exception {
		IDataCenter idc = BeanUtils.toDataCenter(param);
		AppResultBean bean = new AppResultBean();
		MonitorMainBoardVo vo = BeanUtils.jsonToBean(idc, MonitorMainBoardVo.class);
		vo.setAssetId(idc.getParameter("edId") == null ? null : (String) idc.getParameter("edId"));
		try {
			List<MonitorMainBoardVo> list = assetMonitorService.queryMainBoardInfo(vo);
			bean.ok(list);
		} catch (Exception e) {
			logger.error("查询监控器异常", e);
			bean.fail("查询监控器异常");
		}
		return bean;
	}

	/**
	 * 
	 * 查询网卡信息
	 * 
	 */
	@ResponseBody
	@RequestMapping("queryNetCardInfo")
	public AppResultBean queryNetCardInfo(@RequestParam("param") String param, HttpSession session) throws Exception {
		IDataCenter idc = BeanUtils.toDataCenter(param);
		AppResultBean bean = new AppResultBean();
		MonitorNetCardVo vo = BeanUtils.jsonToBean(idc, MonitorNetCardVo.class);
		vo.setAssetId(idc.getParameter("edId") == null ? null : (String) idc.getParameter("edId"));
		try {
			List<MonitorNetCardVo> list = assetMonitorService.queryNetCardInfo(vo);
			bean.ok(list);
		} catch (Exception e) {
			logger.error("查询监控器异常", e);
			bean.fail("查询监控器异常");
		}
		return bean;
	}

	/**
	 * 
	 * 查询硬盘信息
	 * 
	 */
	@ResponseBody
	@RequestMapping("queryDiskInfo")
	public AppResultBean queryDiskInfo(@RequestParam("param") String param, HttpSession session) throws Exception {
		IDataCenter idc = BeanUtils.toDataCenter(param);
		AppResultBean bean = new AppResultBean();
		MonitorDiskVo vo = BeanUtils.jsonToBean(idc, MonitorDiskVo.class);
		vo.setAssetId(idc.getParameter("edId") == null ? null : (String) idc.getParameter("edId"));
		try {
			List<MonitorDiskVo> list = assetMonitorService.queryDiskInfo(vo);
			bean.ok(list);
		} catch (Exception e) {
			logger.error("查询监控器异常", e);
			bean.fail("查询监控器异常");
		}
		return bean;
	}

	/**
	 * 
	 * 查询内存信息
	 * 
	 */
	@ResponseBody
	@RequestMapping("queryMemoryInfo")
	public AppResultBean queryMemoryInfo(@RequestParam("param") String param, HttpSession session) throws Exception {
		IDataCenter idc = BeanUtils.toDataCenter(param);
		AppResultBean bean = new AppResultBean();
		MonitorMemoryVo vo = (MonitorMemoryVo) BeanUtils.jsonToBean(idc, MonitorMemoryVo.class);
		vo.setAssetId(idc.getParameter("edId") == null ? null : (String) idc.getParameter("edId"));
		try {
			List<MonitorMemoryVo> list = assetMonitorService.queryMemoryInfo(vo);
			bean.ok(list);
		} catch (Exception e) {
			logger.error("查询监控器异常", e);
			bean.fail("查询监控器异常");
		}
		return bean;
	}

	/**
	 * 
	 * 查询CPU信息
	 * 
	 */
	@ResponseBody
	@RequestMapping("queryCpuInfo")
	public AppResultBean queryCpuInfo(@RequestParam("param") String param, HttpSession session) throws Exception {
		IDataCenter idc = BeanUtils.toDataCenter(param);
		AppResultBean bean = new AppResultBean();
		MonitorCpuVo vo = (MonitorCpuVo) BeanUtils.jsonToBean(idc, MonitorCpuVo.class);
		vo.setAssetId(idc.getParameter("edId") == null ? null : (String) idc.getParameter("edId"));
		try {
			List<MonitorCpuVo> list = assetMonitorService.queryCpuInfo(vo);
			bean.ok(list);
		} catch (Exception e) {
			logger.error("查询监控器异常", e);
			bean.fail("查询监控器异常");
		}
		return bean;
	}

	/**
	 * 
	 * 查询脆弱性事件
	 * 
	 */
	@ResponseBody
	@RequestMapping("queryLeakList")
	public AppResultBean queryLeakList(@RequestParam("param") String param, HttpSession session) throws Exception {
		IDataCenter idc = BeanUtils.toDataCenter(param);
		AppResultBean bean = new AppResultBean();
		MonitorLeakVo vo = (MonitorLeakVo) BeanUtils.jsonToBean(idc, MonitorLeakVo.class);
		vo.setAssetId(idc.getParameter("edId") == null ? null : (String) idc.getParameter("edId"));
		try {
			List<MonitorLeakVo> list = assetMonitorService.queryLeakList(vo);
			bean.ok(list);
		} catch (Exception e) {
			logger.error("查询监控器异常", e);
			bean.fail("查询监控器异常");
		}
		return bean;
	}

	/**
	 * 
	 * 查询硬件信息
	 * 
	 */
	@ResponseBody
	@RequestMapping("queryHardware")
	public AppResultBean queryHardware(@RequestParam("param") String param, HttpSession session) throws Exception {
		IDataCenter idc = BeanUtils.toDataCenter(param);
		AppResultBean bean = new AppResultBean();
		HardwareVo vo = (HardwareVo) BeanUtils.jsonToBean(idc, HardwareVo.class);
		vo.setAssetId(idc.getParameter("edId") == null ? null : (String) idc.getParameter("edId"));
		try {
			List<HardwareVo> list = assetMonitorService.queryHardware(vo);
			bean.ok(list);
		} catch (Exception e) {
			logger.error("查询监控器异常", e);
			bean.fail("查询监控器异常");
		}
		return bean;
	}

	/**
	 * 
	 * 查询被监控资产端口
	 * 
	 */
	@ResponseBody
	@RequestMapping("queryPortList")
	public AppResultBean queryPortList(@RequestParam("param") String param, HttpSession session) throws Exception {
		IDataCenter idc = BeanUtils.toDataCenter(param);
		AppResultBean bean = new AppResultBean();
		HostPortVO vo = BeanUtils.paramToBean(idc, HostPortVO.class);
		vo.setAssetId(idc.getParameter("edId") == null ? null : (String) idc.getParameter("edId"));
		try {
			List<HostPortVO> list = assetMonitorService.queryPortList(vo);
			bean.ok(list);
		} catch (Exception e) {
			logger.error("查询监控器异常", e);
			bean.fail("查询监控器异常");
		}
		return bean;
	}

	@ResponseBody
	@RequestMapping("queryFileList")
	public AppResultBean queryFileList(@RequestParam("param") String param, HttpSession session) throws Exception {
		IDataCenter idc = BeanUtils.toDataCenter(param);
		AppResultBean bean = new AppResultBean();
		HostFileVO vo = (HostFileVO) BeanUtils.jsonToBean(idc, HostFileVO.class);
		vo.setAssetId(idc.getParameter("edId") == null ? null : (String) idc.getParameter("edId"));
		try {
			List<HostFileVO> list = assetMonitorService.queryFileList(vo);
			bean.ok(list);
		} catch (Exception e) {
			logger.error("查询监控器异常", e);
			bean.fail("查询监控器异常");
		}
		return bean;
	}

	/**
	 * 
	 * 查询被监控资产进程
	 * 
	 */
	@ResponseBody
	@RequestMapping("queryProcessList")
	public AppResultBean queryProcessList(@RequestParam("param") String param, HttpSession session) throws Exception {
		IDataCenter idc = BeanUtils.toDataCenter(param);
		AppResultBean bean = new AppResultBean();
		ProcessVO vo = (ProcessVO) BeanUtils.jsonToBean(idc, ProcessVO.class);
		vo.setAssetId(idc.getParameter("edId") == null ? null : (String) idc.getParameter("edId"));
		try {
			List<ProcessVO> list = assetMonitorService.queryProcessList(vo);
			bean.ok(list);
		} catch (Exception e) {
			logger.error("查询监控器异常", e);
			bean.fail("查询监控器异常");
		}
		return bean;
	}

	/**
	 * 
	 * 根据树节点和树类型设置查询参数
	 * 
	 */
	private void buildQueryParamsByTree(AssetMonitorVO vo, AssetMonitorTreeVO tree) {
		if (AssetMonitorTreeVO.TREE_ROOT_ID.equalsIgnoreCase(tree.getNodeId())) {
			return;
		}
		if (StringUtils.isNotEmpty(tree.getTreeType())) {
			if (AssetMonitorTreeVO.ASSET_TYPE_TREE.equalsIgnoreCase(tree.getTreeType())) {
				if (tree.getNodeId().startsWith(AssetMonitorTreeVO.NODE_TYPE)) {
					vo.setAssetClassId(tree.getNodeId().substring(4));
				} else if (tree.getNodeId().startsWith(AssetMonitorTreeVO.NODE_LEAF)) {
					vo.setAssetTypeId(tree.getNodeId().substring(4));
				}
			} else if (AssetMonitorTreeVO.BUSSINESS_DOMAIN_TREE.equalsIgnoreCase(tree.getTreeType())) {
				vo.setBussinessDomainId(tree.getNodeId());
			} else if (AssetMonitorTreeVO.BUSSINESS_SYSTEM_TREE.equalsIgnoreCase(tree.getTreeType())) {
				vo.setBussinessSystemId(tree.getNodeId().substring(4));
			} else if (AssetMonitorTreeVO.SECURITY_DOMAIN_TREE.equalsIgnoreCase(tree.getTreeType())) {
				vo.setSecurityDomainId(tree.getNodeId());
			}
		}
	}

	/**
	 * 
	 * 查询左侧的树
	 * 
	 */
	@ResponseBody
	@RequestMapping("queryTreeList")
	public AppResultBean queryTreeList(@RequestParam("param") String param, HttpSession session) throws Exception {
		AppResultBean bean = new AppResultBean();
		try {
			TreeVO vo = new TreeVO();
			vo.setPageSize(1000);
			vo.setCurrentPageNum(1);
			vo.setUserId(getUserId(session));
			List<TreeVO> bdTreeList = assetService.queryBussinessDomainTreeList(vo);
			setStoreRowData(bean, "businessDomainTreeStore", bdTreeList);

			List<TreeVO> sdTreeList = assetService.querySecurityDomainTreeList(vo);
			setStoreRowData(bean, "securityDomainTreeStore", sdTreeList);

			List<TreeVO> bsTreeList = assetService.queryBussinessSystemTreeList(vo);
			setStoreRowData(bean, "bussinessSystemTreeStore", bsTreeList);

			List<TreeVO> assetTypeTreeList = assetService.queryAssetTypeTreeList(vo);
			setStoreRowData(bean, "assetTypeTreeStore", assetTypeTreeList);

			List<TreeVO> bdnTreeList = assetService.queryBussinessDomainTreeListNoUnknow(vo);
			setStoreRowData(bean, "businessDomainTreeStoreNoUnknow", bdnTreeList);

			List<TreeVO> sdnTreeList = assetService.querySecurityDomainTreeListNoUnknow(vo);
			setStoreRowData(bean, "securityDomainTreeStoreNoUnknow", sdnTreeList);

			// 查询资产管理左边的树并且统计对应的资产数量
			// AssetMonitorTreeVO tree = new AssetMonitorTreeVO();
			// tree.setPageSize(Integer.MAX_VALUE);
			// tree.setCurrentPageNum(1);
			// tree.setUserId(getUserId(session));
			// // add by liuxu on 20120331:type="zhoushan"时，为舟山项目定制：数据需要过滤用户关注资产
			// String type = (String) idc.getParameter("type");
			// Map<String, List<AssetMonitorTreeVO>> treeMap =
			// assetMonitorService.queryAssetManageTreeListCnt(tree, type);
			// setStoreRowData(bean, "sdTreeWithCnt",
			// treeMap.get("sdTreeWithCnt"));
			// setStoreRowData(bean, "bdTreeWithCnt",
			// treeMap.get("bdTreeWithCnt"));
			// setStoreRowData(bean, "bsTreeWithCnt",
			// treeMap.get("bsTreeWithCnt"));
			// setStoreRowData(bean, "assetTypeTreeWithCnt",
			// treeMap.get("assetTypeTreeWithCnt"));
		} catch (Exception e) {
			logger.error("获取资产左边的树的数据失败", e);
			bean.fail("获取数据失败");
		}
		return bean;
	}

	/**
	 * 
	 * 查询左侧的树
	 * 
	 */
	@ResponseBody
	@RequestMapping("queryCntTreeList")
	public AppResultBean queryCntTreeList(@RequestParam("param") String param, HttpSession session) throws Exception {
		IDataCenter idc = BeanUtils.toDataCenter(param);
		AppResultBean bean = new AppResultBean();
		try {
			AssetMonitorTreeVO tree = new AssetMonitorTreeVO();
			// tree.setUserId(getUserId(session));
			// tree.setCntTree(idc.getParameter("cntTree").toString());
			// 查询资产管理左边的树并且统计对应的资产数量
			tree.setPageSize(Integer.MAX_VALUE);
			tree.setCurrentPageNum(1);
			tree.setUserId(getUserId(session));
			String type = (String) idc.getParameter("type");
			// 安全=3 业务=1 资产=4
			Map<String, List<AssetMonitorTreeVO>> treeMap = assetMonitorService.queryAssetManageTreeListCnt(tree, type);
			setStoreRowData(bean, "sdTreeWithCnt", treeMap.get("sdTreeWithCnt"));
			setStoreRowData(bean, "bdTreeWithCnt", treeMap.get("bdTreeWithCnt"));
			setStoreRowData(bean, "bsTreeWithCnt", treeMap.get("bsTreeWithCnt"));
			setStoreRowData(bean, "assetTypeTreeWithCnt", treeMap.get("assetTypeTreeWithCnt"));
		} catch (AppException e) {
			logger.error("获取资产左边的树的数据失败", e);
			bean.fail("获取数据失败");
		}
		return bean;
	}

	/**
	 * 
	 * 查询资产监控信息
	 * 
	 */
	@ResponseBody
	@RequestMapping("queryMonitor")
	public AppResultBean queryMonitor(@RequestParam("param") String param, HttpSession session) throws Exception {
		IDataCenter idc = BeanUtils.toDataCenter(param);
		AppResultBean bean = new AppResultBean();
		Map<String, Object> assetMonitorMap = new HashMap<String, Object>();//
		List<Object> layerList = new ArrayList<Object>();
		String assetId = idc.getParameter("assetId");
		String assetName = idc.getParameter("assetName");
		MonitorVo vo = new MonitorVo();
		vo.setAssetId(assetId);
		vo.setAssetName(assetName);

		vo.setUserId(getUserId(session));
		// 业务层
		buildBussLayer(layerList, vo);
		// 应用层
		buildAppLayer(layerList, vo);
		// OS层
		buildOSlayer(layerList, vo);
		// 硬件层
		buildHardwareLayer(layerList, vo);
		assetMonitorMap.put("data", layerList);
		bean.ok(assetMonitorMap);
		return bean;
	}

	/**
	 * 
	 * 构建应用层json
	 * 
	 */
	private void buildAppLayer(List<Object> layerList, MonitorVo vo) {
		try {
			// MonitorVo vo = new MonitorVo();
			vo.setPageSize(Integer.MAX_VALUE);
			vo.setCurrentPageNum(1);
			// vo.setAssetId(assetId);
			List<MonitorVo> itemList = assetMonitorService.queryAppMonitor(vo);
			String performCount = itemList.get(0).getPerformCount();
			String faultCount = itemList.get(0).getFaultCount();
			Map<String, Object> appLayerMap = new HashMap<String, Object>();
			List<Object> appMonitorList = new ArrayList<Object>();
			appLayerMap.put("code", AssetMonitorType.APP_LAYER_TYPE);
			appLayerMap.put("label", "应用层");
			appLayerMap.put("iconCls", AssetMonitorCss.APP_CSS);
			appLayerMap.put("items", appMonitorList);
			for (MonitorVo temp : itemList) {
				// 生成监控器
				if (StringUtils.isNotEmpty(temp.getMonitorId())) {
					generateLayerItem(appMonitorList, temp);
				}
			}
			// 性能事件
			if (Integer.parseInt(performCount) > 0) {
				vo.setMonitorType(AssetMonitorType.APP_PERFORM_EVENT_TYPE);
				vo.setMonitorTypeName("性能事件");
				vo.setIconCls(AssetMonitorCss.OS_PERFORM_EVENT_CSS);
				vo.setCount(performCount);
				// vo.setAssetId(assetId);
				vo.setMonitorId(null);
				generateLayerItem(appMonitorList, vo);
			}
			// 故障事件
			if (Integer.parseInt(faultCount) > 0) {
				vo.setMonitorType(AssetMonitorType.APP_FAULT_EVENT_TYPE);
				vo.setMonitorTypeName("故障事件");
				vo.setIconCls(AssetMonitorCss.OS_FAULT_EVENT_CSS);
				vo.setCount(faultCount);
				// vo.setAssetId(assetId);
				vo.setMonitorId(null);
				generateLayerItem(appMonitorList, vo);
			}
			if (appMonitorList.size() > 0) {
				layerList.add(appLayerMap);
			}
		} catch (Exception e) {
			logger.error("查询应用层监控器异常", e);
		}
	}

	/**
	 * 
	 * 构建硬件层json
	 * 
	 */
	private void buildHardwareLayer(List<Object> layerList, MonitorVo vo) {
		Map<String, Object> hardwareLayerMap = new HashMap<String, Object>();
		List<Object> hardwareMonitorList = new ArrayList<Object>();
		hardwareLayerMap.put("code", AssetMonitorType.HARDWARE_LAYER_TYPE);
		hardwareLayerMap.put("label", "硬件层");
		hardwareLayerMap.put("iconCls", AssetMonitorCss.HARDWARE_CSS);
		hardwareLayerMap.put("items", hardwareMonitorList);
		layerList.add(hardwareLayerMap);
		// cpu
		// MonitorVo vo = new MonitorVo();
		vo.setMonitorType(AssetMonitorType.HARDWARE_CPU_TYPE);
		vo.setMonitorTypeName("CPU");
		vo.setIconCls(AssetMonitorCss.HARDWARE_CPU_CSS);
		// vo.setAssetId(assetId);
		generateLayerItem(hardwareMonitorList, vo);
		// memory
		vo.setMonitorType(AssetMonitorType.HARDWARE_MEMORY_TYPE);
		vo.setMonitorTypeName("内存");
		vo.setIconCls(AssetMonitorCss.HARDWARE_MEMORY_CSS);
		// vo.setAssetId(assetId);
		generateLayerItem(hardwareMonitorList, vo);
		// 硬盘
		vo.setMonitorType(AssetMonitorType.HARDWARE_DISK_TYPE);
		vo.setMonitorTypeName("硬盘");
		vo.setIconCls(AssetMonitorCss.HARDWARE_DISK_CSS);
		// vo.setAssetId(assetId);
		generateLayerItem(hardwareMonitorList, vo);
		// 网卡
		vo.setMonitorType(AssetMonitorType.HARDWARE_NET_CARD_TYPE);
		vo.setMonitorTypeName("网卡");
		vo.setIconCls(AssetMonitorCss.HARDWARE_NET_CARD_CSS);
		// vo.setAssetId(assetId);
		generateLayerItem(hardwareMonitorList, vo);
		// //主板
		// vo.setMonitorType(AssetMonitorType.HARDWARE_MAIN_BOARD_TYPE);
		// vo.setMonitorTypeName("主板");
		// vo.setIconCls(AssetMonitorCss.HARDWARE_MAIN_BOARD_CSS);
		//// vo.setAssetId(assetId);
		// generateLayerItem(hardwareMonitorList,vo);
	}

	/**
	 * 
	 * 构建OS层json
	 * 
	 */
	private void buildOSlayer(List<Object> layerList, MonitorVo vo) {
		Map<String, Object> osLayerMap = new HashMap<String, Object>();
		List<Object> osMonitorList = new ArrayList<Object>();
		osLayerMap.put("code", AssetMonitorType.OS_LAYER_TYPE);
		osLayerMap.put("label", "OS层");
		osLayerMap.put("iconCls", AssetMonitorCss.OS_CSS);
		osLayerMap.put("items", osMonitorList);
		layerList.add(osLayerMap);
		String performCount = null;
		String faultCount = null;
		String securityCount = null;
		// OS
		// MonitorVo vo = new MonitorVo();
		vo.setPageSize(Integer.MAX_VALUE);
		vo.setCurrentPageNum(1);
		// vo.setAssetId(assetId);
		try {
			List<MonitorVo> osList = assetMonitorService.queryOSMonitor(vo);
			performCount = osList.get(0).getPerformCount();
			faultCount = osList.get(0).getFaultCount();
			securityCount = assetMonitorService.querySecurityCnt(vo);
			for (MonitorVo temp : osList) {
				if (StringUtils.isNotEmpty(temp.getMonitorType())) {
					// 反射获得CSS样式
					Class<?> c = Class.forName("com.rh.soc.watch.assetmonitor.model.AssetMonitorCss");
					Field f = c.getDeclaredField(temp.getMonitorType());
					vo.setMonitorType(temp.getMonitorType());
					vo.setMonitorTypeName(temp.getMonitorTypeName());
					vo.setMonitorName(temp.getMonitorName());
					vo.setIconCls((String) f.get(null));
					// vo.setAssetId(assetId);
					vo.setCount("");
					vo.setMonitorId(temp.getMonitorId());
					vo.setMonitorManageStatus(temp.getMonitorManageStatus());
					generateLayerItem(osMonitorList, vo);
				}
			}
		} catch (Exception e) {
			logger.error("查询OS层监控器异常", e);
		}
		vo.setMonitorManageStatus(null);
		// 安全事件
		vo.setMonitorType(AssetMonitorType.OS_SECURITY_EVENT_TYPE);
		vo.setMonitorTypeName("安全事件");
		vo.setIconCls(AssetMonitorCss.OS_SECURITY_EVENT_CSS);
		// vo.setAssetId(assetId);
		vo.setMonitorName(null);
		vo.setMonitorId(null);
		if (Integer.parseInt(securityCount) > 0) {
			vo.setCount(securityCount);
		} else {
			vo.setCount("");
		}
		generateLayerItem(osMonitorList, vo);

		// 性能事件
		if (Integer.parseInt(performCount) > 0) {
			vo.setMonitorType(AssetMonitorType.OS_PERFORM_EVENT_TYPE);
			vo.setMonitorTypeName("性能事件");
			vo.setIconCls(AssetMonitorCss.OS_PERFORM_EVENT_CSS);
			vo.setCount(performCount);
			vo.setMonitorName(null);
			// vo.setAssetId(assetId);
			vo.setMonitorId(null);
			generateLayerItem(osMonitorList, vo);
		}

		// 故障事件
		if (Integer.parseInt(faultCount) > 0) {
			vo.setMonitorType(AssetMonitorType.OS_FAULT_EVENT_TYPE);
			vo.setMonitorTypeName("故障事件");
			vo.setIconCls(AssetMonitorCss.OS_FAULT_EVENT_CSS);
			vo.setCount(faultCount);
			vo.setMonitorName(null);
			// vo.setAssetId(assetId);
			vo.setMonitorId(null);
			generateLayerItem(osMonitorList, vo);
		}

		// 系统配置
		vo.setMonitorType(AssetMonitorType.OS_SYSTEM_CONFIG_TYPE);
		vo.setMonitorTypeName("系统配置");
		vo.setIconCls(AssetMonitorCss.OS_SYSTEM_CONFIG_CSS);
		// vo.setAssetId(assetId);
		vo.setCount("");
		vo.setMonitorName(null);
		vo.setMonitorId(null);
		generateLayerItem(osMonitorList, vo);
		// 流量
		vo.setMonitorType(AssetMonitorType.OS_FLOW_TYPE);
		vo.setMonitorTypeName("流量");
		vo.setIconCls(AssetMonitorCss.OS_FLOW_CSS);
		// vo.setAssetId(assetId);
		vo.setCount("");
		vo.setMonitorName(null);
		vo.setMonitorId(null);
		generateLayerItem(osMonitorList, vo);
		// 系统漏洞
		vo.setMonitorType(AssetMonitorType.OS_SYSTEM_LEAK_TYPE);
		vo.setMonitorTypeName("系统漏洞");
		vo.setIconCls(AssetMonitorCss.OS_SYSTEM_LEAK_CSS);
		// vo.setAssetId(assetId);
		vo.setCount("");
		vo.setMonitorName(null);
		vo.setMonitorId(null);
		generateLayerItem(osMonitorList, vo);
		// 基线检查
		vo.setMonitorType(AssetMonitorType.OS_BASE_LINE_TYPE);
		vo.setMonitorTypeName("基线检查");
		vo.setIconCls(AssetMonitorCss.OS_BASE_LINE_CSS);
		// vo.setAssetId(assetId);
		vo.setCount("");
		vo.setMonitorName(null);
		vo.setMonitorId(null);
		generateLayerItem(osMonitorList, vo);
	}

	/**
	 * 
	 * 构建业务层json
	 * 
	 */
	private void buildBussLayer(List<Object> layerList, MonitorVo vo) throws AppException {
		Map<String, Object> bussLayerMap = new HashMap<String, Object>();
		List<Object> bussMonitorList = new ArrayList<Object>();
		bussLayerMap.put("code", AssetMonitorType.BUSS_LAYER_TYPE);
		bussLayerMap.put("label", "业务层");
		bussLayerMap.put("iconCls", AssetMonitorCss.BUSS_CSS);
		bussLayerMap.put("items", bussMonitorList);
		layerList.add(bussLayerMap);
		// URL
		// MonitorVo vo = new MonitorVo();
		vo.setMonitorType(AssetMonitorType.BUSS_URL_TYPE);
		vo.setMonitorTypeName("URL");
		vo.setIconCls(AssetMonitorCss.BUSS_URL_CSS);

		// vo.setAssetId(assetId);
		generateLayerItem(bussMonitorList, vo);
		// port
		vo.setMonitorType(AssetMonitorType.BUSS_PORT_TYPE);
		vo.setMonitorTypeName("端口");
		vo.setIconCls(AssetMonitorCss.BUSS_PORT_CSS);
		// vo.setAssetId(assetId);
		generateLayerItem(bussMonitorList, vo);
		// process
		vo.setMonitorType(AssetMonitorType.BUSS_PROCESS_TYPE);
		vo.setMonitorTypeName("进程");
		vo.setIconCls(AssetMonitorCss.BUSS_PROCESS_CSS);
		// vo.setAssetId(assetId);
		generateLayerItem(bussMonitorList, vo);
		// file
		vo.setMonitorType(AssetMonitorType.BUSS_FILE_TYPE);
		vo.setMonitorTypeName("文件");
		vo.setIconCls(AssetMonitorCss.BUSS_FILE_CSS);
		// vo.setAssetId(assetId);
		generateLayerItem(bussMonitorList, vo);
		String webConsoleUrl = assetMonitorService.getWebConsoleUrl(vo.getAssetId());
		if (StringUtils.isNotEmpty(webConsoleUrl)) {
			vo.setMonitorType(AssetMonitorType.BUSS_WEBCONSOLE_TYPE);
			vo.setMonitorTypeName("Web控制台");
			vo.setIconCls(AssetMonitorCss.BUSS_WEBCONSOLE_CSS);
			vo.setWebConsoleUrl(webConsoleUrl);
			generateLayerItem(bussMonitorList, vo);
		}
	}

	/**
	 * 
	 * 生成每一层的项
	 * 
	 */
	private void generateLayerItem(List<Object> list, MonitorVo vo) {
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("code", vo.getMonitorType());
		map.put("id", vo.getMonitorId());
		map.put("label", vo.getMonitorTypeName());
		map.put("iconCls", vo.getIconCls());
		map.put("count", vo.getCount());
		map.put("insts_status", vo.getInsts_status());
		map.put("assetId", vo.getAssetId());
		map.put("assetName", vo.getAssetName());
		map.put("title", vo.getMonitorName());
		map.put("webConsoleUrl", vo.getWebConsoleUrl());
		map.put("monitorManageStatus", vo.getMonitorManageStatus());
		list.add(map);
	}

	/**
	 * 
	 * 查询巡查日志数据
	 * 
	 */
	@ResponseBody
	@RequestMapping("queryMonitorAssetLog")
	public AppResultBean queryMonitorAssetLog(@RequestParam("param") String param, HttpSession session)
			throws Exception {
		IDataCenter idc = BeanUtils.toDataCenter(param);
		AppResultBean bean = new AppResultBean();
		AssetMonitorVO vo = (AssetMonitorVO) BeanUtils.paramToBean(idc, AssetMonitorVO.class);
		List<AssetMonitorVO> assetMonitorLogList = new ArrayList<AssetMonitorVO>();
		try {
			assetMonitorLogList = assetMonitorService.queryMonitorAssetLog(vo);
			bean.ok(assetMonitorLogList);
		} catch (Exception e) {
			logger.error("获取数据失败", e);
			bean.fail("获取数据失败");
		}
		return bean;
	}

	/**
	 * 
	 * 获取代理id
	 * 
	 */
	@ResponseBody
	@RequestMapping("queryAgentId")
	public AppResultBean queryAgentId(@RequestParam("param") String param, HttpSession session) throws Exception {
		AppResultBean bean = new AppResultBean();
		AssetMonitorVO vo = new AssetMonitorVO();
		try {
			String agentId = assetMonitorService.queryAgentId();
			vo.setAgentId(agentId);
			bean.ok(vo);
		} catch (Exception e) {
			logger.error("获取数据失败", e);
			bean.fail("获取数据失败");
		}
		return bean;
	}

	/**
	 * 
	 * 查询被监控资产
	 * 
	 */
	@ResponseBody
	@RequestMapping("queryMonitorAssetNew")
	public AppResultBean queryMonitorAsset(@RequestParam("param") String param, HttpSession session) throws Exception {
		IDataCenter idc = BeanUtils.toDataCenter(param);
		AppResultBean bean = new AppResultBean();
		AssetMonitorVO vo = BeanUtils.jsonToBean(idc, AssetMonitorVO.class);
		AssetMonitorTreeVO tree = new AssetMonitorTreeVO();
		String nodeId = idc.getParameter("nodeId");
		String treeType = idc.getParameter("treeType");
		tree.setNodeId(nodeId);
		tree.setTreeType(treeType);
		buildQueryParamsByTree(vo, tree);
		try {
			vo.setUserId(getUserId(session));
			List<AssetMonitorVO> list = assetMonitorService.queryMonitorAsset(vo, treeType);
			StringBuffer sb = new StringBuffer();
			if (list != null) {
				for (AssetMonitorVO vox : list) {
					if (sb.length() > 0) {
						sb.append(",");
					}
					sb.append(vox.getAssetId());
				}
				Map<String, BigScreenHostCpuMemVO> vos = bigScreen.getMonitorCpuMemInfoForAllByEds(sb.toString());
				if (vos != null) {
					for (AssetMonitorVO vox : list) {
						BigScreenHostCpuMemVO v = vos.get(vox.getAssetId());
						if(v!=null){
							vox.setHostMonitorId(v.getMonitorId());
							vox.setCpuUsage(v.getCpuUsage());
							vox.setMemUsage(v.getMemoryUsage());
						}
						
					}
				}
			}
			bean.ok(list);
		} catch (Exception e) {
			logger.error("查询监控器异常:" + e.getMessage(), e);
			bean.fail("查询监控器异常");
		}
		return bean;
	}

}
