package com.rh.soc.statistic.model;

import java.sql.ResultSet;
import java.sql.SQLException;

import com.rh.webserver.common.base.bean.BaseVO;

public class TBigScreenFaultLeveMonitorVO extends BaseVO {

	private String edID; // 监控设备名称

	private String edName; // 监控设备名称

	private String faultName;

	private String faultlevel;

	private String faultlevelName;

	private String ipvAddress;

	private String monitorType;

	private String updateDate;

	private int eventType;

	public Object mapRow(ResultSet rs, int arg1) throws SQLException {
		TBigScreenFaultLeveMonitorVO vo = new TBigScreenFaultLeveMonitorVO();
		vo.setRecordCount(super.getRecordCount());
		try {
			vo.setEdID(rs.getString("ed_id")); //
		} catch (Exception e) {
		}

		try {
			vo.setEdName(rs.getString("ed_name")); // 资产名称
		} catch (Exception e) {
		}

		try {
			vo.setFaultName(rs.getString("fault_name"));// 事件名称
		} catch (Exception e) {

		}

		try {
			vo.setFaultlevel(rs.getString("fault_level")); // 故障级别
		} catch (Exception e) {
		}

		try {
			vo.setFaultlevelName(rs.getString("fault_level_name")); // 故障级别
		} catch (Exception e) {
		}

		try {
			vo.setMonitorType(rs.getString("monitor_type")); //
		} catch (Exception e) {
		}

		try {
			vo.setUpdateDate(rs.getString("update_date"));
		} catch (Exception e) {
		}

		try {
			vo.setIpvAddress(rs.getString("ipv_address"));// ip
		} catch (Exception e) {
		}

		try {
			vo.setEventType(rs.getInt("event_type"));
		} catch (Exception e) {
		}

		return vo;
	}

	public String getEdID() {
		return edID;
	}

	public void setEdID(String edID) {
		this.edID = edID;
	}

	public String getEdName() {
		return edName;
	}

	public void setEdName(String edName) {
		this.edName = edName;
	}

	public String getFaultName() {
		return faultName;
	}

	public void setFaultName(String faultName) {
		this.faultName = faultName;
	}

	public String getFaultlevel() {
		return faultlevel;
	}

	public void setFaultlevel(String faultlevel) {
		this.faultlevel = faultlevel;
	}

	public String getFaultlevelName() {
		return faultlevelName;
	}

	public void setFaultlevelName(String faultlevelName) {
		this.faultlevelName = faultlevelName;
	}

	public String getIpvAddress() {
		return ipvAddress;
	}

	public void setIpvAddress(String ipvAddress) {
		this.ipvAddress = ipvAddress;
	}

	public String getMonitorType() {
		return monitorType;
	}

	public void setMonitorType(String monitorType) {
		this.monitorType = monitorType;
	}

	public String getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(String updateDate) {
		this.updateDate = updateDate;
	}

	public int getEventType() {
		return eventType;
	}

	public void setEventType(int eventType) {
		this.eventType = eventType;
	}

}
