package com.rh.soc.statistic.controller;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.PriorityQueue;

import javax.annotation.Resource;
import javax.servlet.http.HttpSession;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.rh.soc.monitor.device.service.IDeviceMonitorService;
import com.rh.soc.statistic.model.TBigScreenFaultLeveMonitorVO;
import com.rh.soc.statistic.service.IBigStatService;
import com.rh.soc.watch.bigscreen.service.IBigScreenService;
import com.rh.soc.watch.technologywatch.securityevent.model.SecurityEventVO;
import com.rh.soc.watch.technologywatch.securityevent.service.ISecurityEventService;
import com.rh.webserver.common.base.bean.AppResultBean;
import com.rh.webserver.common.base.controller.BaseController;
import com.rh.webserver.common.base.exception.AppException;
import com.rh.webserver.common.base.json.IDataCenter;
import com.rh.webserver.common.util.BeanUtils;
import com.rh.webserver.common.util.StringUtil;

@Controller
@RequestMapping("/third/statistic")
public class StatisticThirdInterfaceController extends BaseController {

	static final Log logger = LogFactory.getLog(StatisticThirdInterfaceController.class);

	public final static String SUCCESS = "success";

	@Resource
	ISecurityEventService securityEventService;

	@Resource
	IDeviceMonitorService deviceMonitorService;

	@Resource
	IBigScreenService bigScreenService;

	@Resource
	IBigStatService statService;;

	/**
	 * {查询统计}
	 */
	@ResponseBody
	@RequestMapping("queryItemTypeStatistic")
	public AppResultBean queryItemTypeStatistic(@RequestParam("param") String param, HttpSession session)
			throws Exception {
		logger.info("param=[" + param + "]");
		IDataCenter idc = BeanUtils.toDataCenter(param);
		AppResultBean bean = new AppResultBean();
		try {
			SecurityEventVO secVo = BeanUtils.jsonToBean(idc, SecurityEventVO.class);
			String enterDate = idc.getParameter("enterDate");
			if (StringUtil.isNotEmpty(enterDate)) {
				secVo.setEnterDate(enterDate);
			}
			secVo.setUserId(getUserId(session));// -- 添加安全域权限控制
			secVo.setTypeid(ISecurityEventService.STAT_ITEM_TYPE);
			List<Map<String, Object>> list = securityEventService.statQueryForItemByTime(secVo);
			bean.setObj(list);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			bean.fail("获取数据失败");
		}
		return bean;
	}

	@RequestMapping("queryDeviceLevelStatus")
	@ResponseBody
	public AppResultBean queryDeviceLevelStatus(@RequestParam(value = "param", required = false) String param,
			HttpSession session) {
		AppResultBean bean = new AppResultBean();
		try {
			TBigScreenFaultLeveMonitorVO vo = new TBigScreenFaultLeveMonitorVO();
			String userId = getUserId(session);
			vo.setUserId(userId);
			vo.setCurrentPageNum(1);
			vo.setPageSize(10);
			List<TBigScreenFaultLeveMonitorVO> result = statService.getMonitorFaultLevelForEds(vo);
			bean.ok(result);
		} catch (AppException e) {
			e.printStackTrace();
			bean.fail("异常");
		}
		return bean;
	}

	@RequestMapping("queryDeviceLevelStatusTotal")
	@ResponseBody
	public AppResultBean queryDeviceLevelStatusTotal(@RequestParam(value = "param", required = false) String param,
			HttpSession session) {
		AppResultBean bean = new AppResultBean();
		try {
			TBigScreenFaultLeveMonitorVO vo = new TBigScreenFaultLeveMonitorVO();
			String userId = getUserId(session);
			vo.setUserId(userId);
			vo.setCurrentPageNum(1);
			vo.setPageSize(10);
			List<TBigScreenFaultLeveMonitorVO> result = statService.getMonitorFaultLevelForEds(vo);
			List<TBigScreenFaultLeveMonitorVO> result2 = statService.getDataCenterMonitorPerfEvent(vo);

			if (result == null) {
				result = new ArrayList<>();
			}
			if (result2 == null) {
				result2 = new ArrayList<>();
			}

			final SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			PriorityQueue<TBigScreenFaultLeveMonitorVO> queue = new PriorityQueue<>(20,
					new Comparator<TBigScreenFaultLeveMonitorVO>() {
						@Override
						public int compare(TBigScreenFaultLeveMonitorVO paramT1, TBigScreenFaultLeveMonitorVO paramT2) {
							String t = paramT1.getUpdateDate();
							String t2 = paramT2.getUpdateDate();
							try {
								long tx1 = sdf.parse(t).getTime();
								long tx2 = sdf.parse(t2).getTime();
								if (tx1 > tx2) {
									return -1;
								} else if (tx1 < tx2) {
									return 1;
								} else {
									return 0;
								}
							} catch (ParseException e) {
								e.printStackTrace();
								return 0;
							}
						}
					});
			queue.addAll(result);
			queue.addAll(result2);
			List<TBigScreenFaultLeveMonitorVO> r = new ArrayList<>();
			r.addAll(queue);
			bean.ok(r);
		} catch (AppException e) {
			e.printStackTrace();
			bean.fail("异常");
		}
		return bean;
	}
}