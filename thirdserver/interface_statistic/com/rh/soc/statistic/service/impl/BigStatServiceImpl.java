package com.rh.soc.statistic.service.impl;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.rh.soc.statistic.dao.IBigStatDao;
import com.rh.soc.statistic.model.TBigScreenFaultLeveMonitorVO;
import com.rh.soc.statistic.service.IBigStatService;
import com.rh.webserver.common.base.exception.AppException;
import com.rh.webserver.common.base.service.BaseService;

@Service
public class BigStatServiceImpl extends BaseService implements IBigStatService {

	@Resource
	IBigStatDao dao;

	@Override
	public List<TBigScreenFaultLeveMonitorVO> getDataCenterMonitorPerfEvent(
			TBigScreenFaultLeveMonitorVO faultAlarmEventVO) {
		return dao.getDataCenterMonitorPerfEvent(faultAlarmEventVO);
	}

	public List<TBigScreenFaultLeveMonitorVO> getMonitorFaultLevelForEds(TBigScreenFaultLeveMonitorVO faultAlarmEventVO)
			throws AppException {
		try {
			return dao.getMonitorFaultLevelForEds(faultAlarmEventVO);
		} catch (Exception e) {
			logger.error("获取数据失败", e);
			throw new AppException(e);
		}
	}

}
