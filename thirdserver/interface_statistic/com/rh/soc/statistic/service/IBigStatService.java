package com.rh.soc.statistic.service;

import java.util.List;

import com.rh.soc.statistic.model.TBigScreenFaultLeveMonitorVO;
import com.rh.webserver.common.base.exception.AppException;

public interface IBigStatService {
	public List<TBigScreenFaultLeveMonitorVO> getDataCenterMonitorPerfEvent(
			TBigScreenFaultLeveMonitorVO faultAlarmEventVO);

	public List<TBigScreenFaultLeveMonitorVO> getMonitorFaultLevelForEds(TBigScreenFaultLeveMonitorVO faultAlarmEventVO)
			throws AppException;
}
