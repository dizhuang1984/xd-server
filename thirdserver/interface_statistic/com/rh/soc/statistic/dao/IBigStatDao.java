package com.rh.soc.statistic.dao;

import java.util.List;

import com.rh.soc.statistic.model.TBigScreenFaultLeveMonitorVO;

public interface IBigStatDao {

	public List<TBigScreenFaultLeveMonitorVO> getDataCenterMonitorPerfEvent(
			TBigScreenFaultLeveMonitorVO faultAlarmEventVO);

	public List<TBigScreenFaultLeveMonitorVO> getMonitorFaultLevelForEds(
			TBigScreenFaultLeveMonitorVO faultAlarmEventVO);
}
