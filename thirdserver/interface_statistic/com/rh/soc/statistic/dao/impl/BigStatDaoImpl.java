package com.rh.soc.statistic.dao.impl;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.rh.soc.statistic.dao.IBigStatDao;
import com.rh.soc.statistic.model.TBigScreenFaultLeveMonitorVO;
import com.rh.webserver.common.base.auth.DataAuth;
import com.rh.webserver.common.base.dao.BaseDao;

@Repository
public class BigStatDaoImpl extends BaseDao implements IBigStatDao {

	/**
	 * 故障事件大屏展示
	 */
	public List<TBigScreenFaultLeveMonitorVO> getMonitorFaultLevelForEds(
			TBigScreenFaultLeveMonitorVO faultAlarmEventVO) {

		StringBuffer sql = new StringBuffer(1024);

		sql.append("SELECT t1.ed_id,                                                                    ");
		sql.append(
				"		         td.ed_name,  2 as event_type,                                                     ");
		sql.append("		         t2.fault_name,                                                               ");
		sql.append("		         tmm.monitor_type,                                                            ");
		sql.append("		         MAX (C.IPV_ADDRESS) IPV_ADDRESS,                                             ");
		sql.append("	 DECODE(MAX(T1.EVENT_LEVEL),-1,MAX(T2.FAULT_LEVEL),MAX(T1.EVENT_LEVEL)) fault_level,        ");
		sql.append(
				"	 DECODE(DECODE(MAX(T1.EVENT_LEVEL),-1,MAX(T2.FAULT_LEVEL),MAX(T1.EVENT_LEVEL)), -1, '未知', 0, '很高',1, '高',2, '中',3, '低',4, '很低') fault_level_name,   ");
		sql.append("		         MAX (TO_CHAR (t1.update_date, 'yyyy-mm-dd hh24:mi:ss')) update_date          ");
		sql.append("		    FROM t_fault_event T1                                                             ");
		sql.append("		         JOIN (  SELECT td.fault_no, MAX (td.enter_date) enter_date                   ");
		sql.append("		                   FROM t_fault_event_detail td                                       ");
		sql.append("		               GROUP BY td.fault_no) tfd                                              ");
		sql.append("		            ON t1.fault_no = tfd.fault_no                                             ");
		sql.append("		         JOIN t_fault_dict T2 ON T1.fault_ID = T2.fault_ID                            ");
		sql.append("		         JOIN t_fault_class T3 ON T2.CLASS = T3.CLASS                                 ");
		sql.append("		         LEFT JOIN t_domain_dict sd ON T1.Sdoma_Id = sd.doma_id                       ");
		sql.append("		         LEFT JOIN t_domain_dict bd ON T1.Bdoma_Id = bd.doma_id                       ");
		sql.append("		         LEFT JOIN t_monitor_master tmm ON t1.monitor_id = tmm.monitor_id             ");
		sql.append("		         LEFT JOIN t_monitor_type g ON tmm.monitor_type = g.monitor_type              ");
		sql.append("		         LEFT JOIN t_entity_device td ON td.ed_id = T1.ed_id                          ");
		sql.append("		         LEFT JOIN t_ed_ip_address c ON td.ed_id = c.ed_id AND c.key_ip = 1           ");
		sql.append("		   WHERE     1 = 1                                                                    ");
		sql.append("		         AND td.ed_name IS NOT NULL                                                   ");
		sql.append("		         AND tmm.monitor_type IS NOT NULL                                             ");
		sql.append("		         AND tmm.monitor_name IS NOT NULL                                             ");
		sql.append("		         AND t1.fault_no IN (SELECT a1.fault_no                                       ");
		sql.append("		                               FROM t_ed_vs_domain a2,                                ");
		sql.append("		                                    t_fault_vs_ed a1,                                 ");
		sql.append("		                                    t_user_vs_grants vsg                              ");
		sql.append("		                              WHERE     a2.ed_id = a1.ed_id                           ");
		sql.append("		                                    AND a2.doma_class = 1                             ");
		sql.append("		                                    AND a2.doma_id = vsg.grants_code)                 ");

		if (DataAuth.needDataAuth()) {
			sql.append(" AND vsg.psn_id = '" + faultAlarmEventVO.getUserId() + "' ");
		}
		sql.append("		         AND t1.current_status IN ('0')                                               ");
		sql.append("		         AND T1.fault_status = 1                                                      ");
		sql.append("		         AND td.ed_status = 0                                                         ");
		sql.append("		         AND EXISTS                                                                   ");
		sql.append("		                (SELECT 'X'                                                           ");
		sql.append("		                   FROM t_ed_ip_address t6, t_fault_vs_ed fveip                       ");
		sql.append("		                  WHERE fveip.ed_id = t6.ed_id AND t1.fault_no = fveip.fault_no)      ");
		sql.append("		GROUP BY td.ed_name,                                                                  ");
		sql.append("		         t1.ed_id,                                                                    ");
		sql.append("		         tmm.monitor_type,                                                            ");
		sql.append("		         t2.fault_name                                                                ");
		sql.append("		ORDER BY update_date DESC                                                             ");
		return query(sql.toString(), faultAlarmEventVO);
	}

	public List<TBigScreenFaultLeveMonitorVO> getDataCenterMonitorPerfEvent(
			TBigScreenFaultLeveMonitorVO faultAlarmEventVO) {

		StringBuffer sql = new StringBuffer(1024);

		sql.append("SELECT t1.ed_id,                                                                    ");
		sql.append(
				"		         td.ed_name,  3 as event_type,                                                     ");
		sql.append(
				"		         t2.perf_name as fault_name,                                                               ");
		sql.append("		         tmm.monitor_type,                                                            ");
		sql.append("		         MAX (C.IPV_ADDRESS) IPV_ADDRESS,                                             ");
		sql.append("	 DECODE(MAX(T1.EVENT_LEVEL),-1,MAX(T2.PERF_LEVEL),MAX(T1.EVENT_LEVEL)) fault_level,        ");
		sql.append(
				"	 DECODE(DECODE(MAX(T1.EVENT_LEVEL),-1,MAX(T2.PERF_LEVEL),MAX(T1.EVENT_LEVEL)), -1, '未知', 0, '很高',1, '高',2, '中',3, '低',4, '很低') fault_level_name,   ");
		sql.append("		         MAX (TO_CHAR (t1.update_date, 'yyyy-mm-dd hh24:mi:ss')) update_date          ");
		sql.append(
				"		    FROM t_performance_event T1                                                             ");
		sql.append(
				"		         JOIN (  SELECT td.performance_no, MAX (td.enter_date) enter_date                   ");
		sql.append(
				"		                   FROM t_performance_event_detail td                                       ");
		sql.append(
				"		               GROUP BY td.performance_no) tfd                                              ");
		sql.append(
				"		            ON t1.performance_no = tfd.performance_no                                             ");
		sql.append(
				"		         JOIN t_performance_dict T2 ON T1.Perf_Id = T2.Perf_Id                            ");
		sql.append(
				"		         JOIN t_performance_class T3 ON T2.CLASS = T3.CLASS                                 ");
		sql.append("		         LEFT JOIN t_domain_dict sd ON T1.Sdoma_Id = sd.doma_id                       ");
		sql.append("		         LEFT JOIN t_domain_dict bd ON T1.Bdoma_Id = bd.doma_id                       ");
		sql.append("		         LEFT JOIN t_monitor_master tmm ON t1.monitor_id = tmm.monitor_id             ");
		sql.append("		         LEFT JOIN t_monitor_type g ON tmm.monitor_type = g.monitor_type              ");
		sql.append("		         LEFT JOIN t_entity_device td ON td.ed_id = T1.ed_id                          ");
		sql.append("		         LEFT JOIN t_ed_ip_address c ON td.ed_id = c.ed_id AND c.key_ip = 1           ");
		sql.append("		   WHERE     1 = 1                                                                    ");
		sql.append("		         AND td.ed_name IS NOT NULL                                                   ");
		sql.append("		         AND tmm.monitor_type IS NOT NULL                                             ");
		sql.append("		         AND tmm.monitor_name IS NOT NULL                                             ");
		sql.append(
				"		         AND t1.performance_no IN (SELECT a1.performance_no                                       ");
		sql.append("		                               FROM t_ed_vs_domain a2,                                ");
		sql.append("		                                    t_perf_vs_ed a1,                                 ");
		sql.append("		                                    t_user_vs_grants vsg                              ");
		sql.append("		                              WHERE     a2.ed_id = a1.ed_id                           ");
		sql.append("		                                    AND a2.doma_class = 1                             ");
		sql.append("		                                    AND a2.doma_id = vsg.grants_code)                 ");

		if (DataAuth.needDataAuth()) {
			sql.append(" AND vsg.psn_id = '" + faultAlarmEventVO.getUserId() + "' ");
		}
		sql.append("		         AND t1.current_status IN ('0')                                               ");
		sql.append("		         AND T1.perf_status = 1                                                      ");
		sql.append("		         AND td.ed_status = 0                                                         ");
		sql.append("		         AND EXISTS                                                                   ");
		sql.append("		                (SELECT 'X'                                                           ");
		sql.append("		                   FROM t_ed_ip_address t6, t_perf_vs_ed fveip                       ");
		sql.append(
				"		                  WHERE fveip.ed_id = t6.ed_id AND t1.performance_no = fveip.performance_no)      ");
		sql.append("		GROUP BY td.ed_name,                                                                  ");
		sql.append("		         t1.ed_id,                                                                    ");
		sql.append("		         tmm.monitor_type,                                                            ");
		sql.append("		         t2.perf_name                                                                ");
		sql.append("		ORDER BY update_date DESC                                                             ");
		return query(sql.toString(), faultAlarmEventVO);
	}
}
