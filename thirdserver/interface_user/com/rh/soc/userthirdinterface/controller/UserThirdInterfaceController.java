package com.rh.soc.userthirdinterface.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.rh.soc.management.usermanage.usermanage.model.UserInfoVO;
import com.rh.soc.management.usermanage.usermanage.service.IUserManageService;
import com.rh.webserver.common.base.bean.AppResultBean;
import com.rh.webserver.common.base.controller.BaseController;
import com.rh.webserver.common.base.exception.AppException;
import com.rh.webserver.common.base.json.IDataCenter;
import com.rh.webserver.common.util.BeanUtils;
import com.rh.webserver.common.util.StringUtil;

@Controller
@RequestMapping("third/user")
public class UserThirdInterfaceController extends BaseController {

	@Resource
	IUserManageService userManageService;

	/**
	 * 添加用户
	 */
	@ResponseBody
	@RequestMapping("batchAddUser")
	public AppResultBean batchAddUser(@RequestParam("param") String param, HttpSession session, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		logger.info("param=["+param+"]");
		IDataCenter idc = BeanUtils.toDataCenter(param);
		AppResultBean bean = new AppResultBean();
		Map<String, AppResultBean> resultMap = new HashMap<String, AppResultBean>();
		List<UserInfoVO> userInfoList = (List<UserInfoVO>) BeanUtils.jsonToBeans(idc, "userInfos", UserInfoVO.class);

		try {
			for (int i = 0; i < userInfoList.size(); i++) {
				UserInfoVO vo = userInfoList.get(i);
				vo.setRoleType("3");//设置默认的roleType
				if (vo.getUserDept() == null){
					vo.setUserDept("");
				}
				
				String userAccount=vo.getUserAccount();
				if(StringUtil.isEmpty(userAccount)){
					AppResultBean resultBean = new AppResultBean();
					resultBean.fail("账号为空");
					resultMap.put(vo.getUserName()+"--"+System.currentTimeMillis(), resultBean);
					continue;
				}
				
				String userName=vo.getUserName();
				if(StringUtil.isEmpty(userName)){
					AppResultBean resultBean = new AppResultBean();
					resultBean.fail("用户名为空");
					resultMap.put(vo.getUserAccount(), resultBean);
					continue;
				}
				

				String userPwdXor = vo.getUserPwd();
				if(StringUtil.isEmpty(userPwdXor)){
					AppResultBean resultBean = new AppResultBean();
					resultBean.fail("密码为空");
 					resultMap.put(vo.getUserAccount(), resultBean);
 					continue;
				}
				
//				String userPwd = Xor.Encrypt(userPwdXor);
//				vo.setUserPwd(Base64.encode(DisgestUtil.md5(userPwd).getBytes()));
				vo.setUserPwd(userPwdXor);
				bean.ctxput("userAccount", vo.getUserAccount());
				int rtn = userManageService.addUserInfoForInterface(vo);
				if (rtn == -1) {
					AppResultBean resultBean = new AppResultBean();
					resultBean.fail("账号已经存在。");
					resultMap.put(vo.getUserAccount(), resultBean);
				} 
				 else {
//					refreshCodelist("allUserList");
//					refreshCodelist("userListCodeList");
					AppResultBean resultBean = new AppResultBean();
					resultBean.ok("用户增加同步成功。");
					resultMap.put(vo.getUserAccount(), resultBean);
				}
			}
		} catch (AppException e) {
			logger.error(e.getMessage(), e);
			bean.fail("添加用户失败");
		}
		bean.ok(resultMap);
		return bean;
	}
	
	
	/**
	 * 添加用户
	 */
	@ResponseBody
	@RequestMapping("batchUpdUser")
	public AppResultBean batchUpdUser(@RequestParam("param") String param, HttpSession session, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		logger.info("param=["+param+"]");
		IDataCenter idc = BeanUtils.toDataCenter(param);
		AppResultBean bean = new AppResultBean();
		Map<String, AppResultBean> resultMap = new HashMap<String, AppResultBean>();
		List<UserInfoVO> userInfoList = (List<UserInfoVO>) BeanUtils.jsonToBeans(idc, "userInfos", UserInfoVO.class);

		try {
			for (int i = 0; i < userInfoList.size(); i++) {
				UserInfoVO vo = userInfoList.get(i);
				vo.setRoleType("3");//设置默认的roleType
				if (vo.getUserDept() == null){
					vo.setUserDept("");
				}
				
				String userAccount=vo.getUserAccount();
				if(StringUtil.isEmpty(userAccount)){
					AppResultBean resultBean = new AppResultBean();
					resultBean.fail("账号为空");
					resultMap.put(vo.getUserName()+"--"+System.currentTimeMillis(), resultBean);
					continue;
				}
				
				String userName=vo.getUserName();
				if(StringUtil.isEmpty(userName)){
					AppResultBean resultBean = new AppResultBean();
					resultBean.fail("用户名为空");
					resultMap.put(vo.getUserAccount(), resultBean);
					continue;
				}
				
				String userPwdXor=idc.getParameter("userPwd");
				if(!StringUtil.isEmpty(userPwdXor)){
					vo.setUserPwd(userPwdXor);
				}

//				if(StringUtil.isEmpty(userPwdXor)){
//					AppResultBean resultBean = new AppResultBean();
//					resultBean.fail("密码为空");
// 					resultMap.put(vo.getUserAccount(), resultBean);
// 					continue;
//				}
				
//				String userPwd = Xor.Encrypt(userPwdXor);
//				vo.setUserPwd(Base64.encode(DisgestUtil.md5(userPwd).getBytes()));
				
				bean.ctxput("userAccount", vo.getUserAccount());
				int rtn = userManageService.updUserInfoForInterface(vo);
				if (rtn == -1) {
					AppResultBean resultBean = new AppResultBean();
					resultBean.fail("账号不存在");
					resultMap.put(vo.getUserAccount(), resultBean);
				} 
				 else {
//					refreshCodelist("allUserList");
//					refreshCodelist("userListCodeList");
					AppResultBean resultBean = new AppResultBean();
					resultBean.ok("用户修改同步成功。");
					resultMap.put(vo.getUserAccount(), resultBean);
				}
			}
		} catch (AppException e) {
			logger.error(e.getMessage(), e);
			bean.fail("添加用户失败");
		}
		bean.ok(resultMap);
		return bean;
	}

	/**
	 * 删除用户
	 */
	@ResponseBody
	@RequestMapping("batchDelUser")
	public AppResultBean delUser(@RequestParam("param") String param, HttpSession session, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		IDataCenter idc = BeanUtils.toDataCenter(param);
		AppResultBean bean = new AppResultBean();
		
		logger.info("param=["+param+"]");
		
		String users = idc.getParameter("users").toString();
		String[] userBuf=users.split(",");
		
		Map<String, AppResultBean> resultMap = new HashMap<String, AppResultBean>();
	
		try {
			for (int k = 0; k < userBuf.length; k++) {
				UserInfoVO vo = new UserInfoVO();
				String userAccount = userBuf[k];
				vo.setUserAccount(userAccount);
				int rtn=userManageService.delUserinfoForInterface(vo);
				if (rtn == -1) {
					AppResultBean resultBean = new AppResultBean();
					resultBean.fail("账号不存在");
					resultMap.put(vo.getUserAccount(), resultBean);
					continue;
				} 
				if (rtn == -2) {
					AppResultBean resultBean = new AppResultBean();
					resultBean.fail("删除失败");
					resultMap.put(vo.getUserAccount(), resultBean);
					continue;
				} 
				 else {
					AppResultBean resultBean = new AppResultBean();
					resultBean.ok("用户删除同步成功。");
					resultMap.put(vo.getUserAccount(), resultBean);
					continue;
				}
//				CodeListManager.getCodeListManager().refresh("allUserList");
//				CodeListManager.getCodeListManager().refresh("userListCodeList");
			}
		} catch (AppException e) {
			e.printStackTrace();
			bean.fail(e.getMessage());
		}

		bean.ok(resultMap);
		return bean;
	}

}
