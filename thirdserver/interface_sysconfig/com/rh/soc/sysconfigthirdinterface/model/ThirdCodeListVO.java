package com.rh.soc.sysconfigthirdinterface.model;
import java.io.Serializable;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.rh.webserver.common.base.bean.BaseVO;

public class ThirdCodeListVO  extends BaseVO implements Serializable {

	private String code;

	private String name;

	private String parentCode;

	
	
	public String getCode() {
		return code;
	}



	public void setCode(String code) {
		this.code = code;
	}



	public String getName() {
		return name;
	}



	public void setName(String name) {
		this.name = name;
	}



	public String getParentCode() {
		return parentCode;
	}



	public void setParentCode(String parentCode) {
		this.parentCode = parentCode;
	}



	/**
	 * 
	 * {方法功能中文描述}
	 * 
	 * @param rs
	 * @param i
	 * @return
	 * @throws SQLException
	 */
	public Object mapRow(ResultSet rs, int i) throws SQLException {
		ThirdCodeListVO vo = new ThirdCodeListVO();
		return vo;
	}

}
