package com.rh.soc.zonethirdinterface.dao;

import java.util.List;
import java.util.Map;

import com.rh.soc.bussinessdomainmanage.model.TreeVO;
import com.rh.soc.securitydomainmanage.model.SecurityDomainBriefVO;
import com.rh.soc.securitydomainmanage.model.SecurityDomainVO;
import com.rh.webserver.common.base.exception.AppException;

/**
 * 





 * 


 */
public interface IThirdSecurityDomainDAO {
	/**
	 * 
	 * 添加安全域
	 * 
	 * @param vo
	 * @return

	 */
	public int addSecurityDomain(SecurityDomainVO vo);

	/**
	 * 根据条件查询树形数据
	 */
	public List<SecurityDomainBriefVO> queryTreeWithCondition(SecurityDomainBriefVO vo) throws AppException;

	public List<SecurityDomainVO> queryAllTree();
	
	/**
	 * 
	 * 返回所有安全域
	 * @return
	 */
	public List<SecurityDomainBriefVO> queryAllTreeForBrief();
	
	/**
	 * 查找指定节点的子节点
	 * @return
	 */
	public List<SecurityDomainBriefVO> queryTreeChildrenList(SecurityDomainBriefVO vo);

	/**
	 * 
	 * 查询安全域信息
	 * 
	 * @param vo
	 * @return

	 */
	public List<SecurityDomainVO> queryAllSecurityDomain(SecurityDomainVO vo);

	/**
	 * 
	 * 批量删除安全域
	 * 
	 * @param vo
	 * @return

	 */

	public int deleteSecurityDomain(SecurityDomainVO vo);

	/**
	 * 
	 * {删除用户安全域的数据权限}
	 * 
	 * @param vo
	 * @return

	 */
	public int delUserVsGrants(SecurityDomainVO vo);

	/**
	 * 
	 * 修改安全域
	 * 
	 * @param vo
	 * @return

	 */

	public int updateSecurityDomain(SecurityDomainVO vo);

	/**
	 * 
	 * 根据系统名称和local状态在t_local_dict中查出local_code
	 * 
	 * @param localStatus
	 * @param localName
	 * @return

	 */

	public String queryLocal(String localStatus, String localName);

	/**
	 * 
	 * 根据安全域id 和安全域名称在t_staff_structure中插入一条记录
	 * 
	 * @param doma_id
	 * @param doma_name
	 * @return

	 */

	public String addStaffStructure(String doma_id, String doma_name);

	/**
	 * 
	 * 根据安全域id 和页面上选择的域代理向t_domain_vs_agent中插入一条记录
	 * 
	 * @param doma_id
	 * @param doma_agent
	 * @return

	 */

	public int addDomainVsAgent(String domaId, String nodeId);

	/**
	 * 
	 * 根据domaId查询具体信息
	 * 
	 * @param domaId
	 * @return

	 */

	public SecurityDomainVO querySecurityDetail(String domaId);

	/**
	 * 
	 * 根据domaId删除domain_vs_agent中的记录，目的在于用于更新这个表的nodeId
	 * 
	 * @param domaId

	 */

	public void delDomainVsAgent(String domaId);

	/**
	 * 
	 * 判断安全域名称是否存在
	 * 
	 * @param domaName
	 * @param pdomaId
	 * @return

	 */

	public int queryDomaName(String domaName, String pdomaId, String domaId);

	/**
	 * 
	 * 判断安全域简称是否存在
	 * 
	 * @param domaAbbreviation
	 * @param pdomaId
	 * @return

	 */

	public int queryDomaAbbreviation(String domaAbbreviation, String pdomaId, String domaId);

	/**
	 * 
	 * 查询代理引擎id
	 * 
	 * @param domaId
	 * @return

	 */

	public String queryNodeId(String domaId);

	/**
	 * 
	 * 查询代理引擎name
	 * 
	 * @param domaId
	 * @return

	 */

	public String queryNodeName(String domaId);

	/**
	 * 
	 * 查询隶属安全域的名称
	 * 
	 * @param pdomaId
	 * @return

	 */

	public String queryPdomaName(String pdomaId);

	/**
	 * 
	 * 修改t_staff_structure中的ss_name （安全域名称）
	 * 
	 * @param ssName
	 * @param domaId
	 * @return

	 */
	public int updateStaffStructure(String ssName, String domaId);

	/**
	 * 
	 * 查询角色信息是否存在
	 * 
	 * @param secureDomainvo
	 * @return

	 */
	public String queryRoleIsExists(SecurityDomainVO secureDomainvo);

	/**
	 * 
	 * 查询该域下的所有Id值
	 * 
	 * @param secureDomainvo
	 * @return

	 */
	public SecurityDomainVO queryALLInfoId(SecurityDomainVO secureDomainvo);

	/**
	 * 
	 * 查询人员和用户信息
	 * 
	 * @param secureDomainvo
	 * @return

	 */
	public List<SecurityDomainVO> queryStaffAndUser(SecurityDomainVO secureDomainvo);

	/**
	 * 
	 * 检查该域是否存在
	 * 
	 * @param domaId
	 * @return

	 */
	public int checkSecurityIsExists(String domaId);

	/**
	 * 
	 * 判断同一代理引擎下的网段是否重复
	 * 
	 * @param vo
	 * @return

	 */
	public List<SecurityDomainVO> queryIsExistIp(SecurityDomainVO vo);

	/**
	 * 
	 * 判断该域下是否存在子域
	 * 
	 * @param vo
	 * @return

	 */
	public List<SecurityDomainVO> isExistsChildDomain(SecurityDomainVO vo);

	/**
	 * 
	 * 查询所有根节点下的域
	 * 
	 * @param vo
	 * @return

	 */
	public List<SecurityDomainVO> queryRootDomain(SecurityDomainVO vo);

	/**
	 * 
	 * 查询某个域下的子域
	 * 
	 * @param voobj
	 * @return

	 */
	public List<SecurityDomainVO> querySubDomain(SecurityDomainVO voobj);

	/**
	 * 
	 * 查询子域下的Id信息（Sql递归查询）
	 * 
	 * @param vo
	 * @return

	 */
	public String queryChildSecDomain(SecurityDomainVO vo);

	/**
	 * 
	 * 查询该域下是否有资产
	 * 
	 * @param domaId
	 * @return

	 */
	public String isHasDevice(String domaId);

	/**
	 * 
	 * 查询子域下的Id信息
	 * 
	 * @param vo
	 * @return

	 */
	public String queryChildDomain(SecurityDomainVO vo);

	/**
	 * 
	 * 添加域时需要向有关扫描区域的两张表添加数据
	 * 
	 * @param vo
	 * @return

	 */
	public int addScanZone(SecurityDomainVO vo);

	/**
	 * 
	 * 删除域时同时删除扫描区域表中的内容
	 * 
	 * @param vo
	 * @return

	 */
	public int delScanZone(SecurityDomainVO vo);

	/**
	 * 
	 * 修改安全域时修改扫描区域表信息
	 * 
	 * @param domaName
	 * @param domaId
	 * @return

	 */
	public int updScanZone(String domaName, String domaId);

	/**
	 * 
	 * 获取安全域树信息
	 * 
	 * @param vo
	 * @return

	 */
	public List<TreeVO> querySecurityDomainTree(TreeVO vo);

	/**
	 * 
	 * {删除扫描区域表时同时删除t_auto_topo_network t_auto_discover_network两张表的信息
	 * 
	 * 
	 * @return

	 */
	public int delNetWork(SecurityDomainVO vo);

	/**
	 * 
	 * {修改代理服务器类型表信息}
	 * 
	 * @return

	 */
	public int updateAgentType();

	/**
	 * 
	 * 向扫描区域添加域名称，将域名称已全路径的形式查询出来
	 * 
	 * @param domaId
	 * @return

	 */
	public String querySDNamePath(String pdomaId);

	/**
	 * 
	 * {查询单个安全域的层级}
	 * 
	 * @param domaId
	 * @return

	 */
	public String querySingleDomaLevel(String domaId);

	/**
	 * 
	 * 批量删除t_domain_vs_agent中的记录
	 * 
	 * @param vo

	 */
	public int delDomainVsAgentBatch(SecurityDomainVO vo);

	// /**
	// *
	// * 新增安全域的gis坐标信息
	// * @param vo
	// * @return

	// */
	// public int addGis(SecurityDomainVO vo);
	//	
	//	
	// /**
	// *
	// * 删除安全域的gis坐标信息
	// * @param vo
	// * @return

	// */
	// public int delGis(SecurityDomainVO vo);
	
	/**
	 * 
	 * 查询子域下的Id信息
	 * 
	 * @param vo
	 * @return

	 */
	public String checkChildDomain(SecurityDomainVO vo);
	
	/**
	 * 
	 * {获取安全域下一个权值编号}
	 * 
	 * @param vo
	 * @return

	 */
	public String queryNextSort(SecurityDomainVO vo);
	
	/**
	 * 
	 * {获取当前域的权重}
	 * 
	 * @param vo
	 * @return

	 */
	public String queryDomaSort(SecurityDomainVO vo);
	
	/**
	 * 
	 * {获取需要互换位置的域的信息}
	 * 
	 * @param vo
	 * @return

	 */
	public Map<String, String> queryChangeDoma(SecurityDomainVO vo);
	
	/**
	 * 
	 * {修改需要互换位置的域信息}
	 * 
	 * @param list
	 * @return

	 */
	public int updDomaSort(List<Map<String, String>> list);
	
	/**
	 * 
	 * {根据域类型查询域的树结构}
	 * 
	 * @return

	 */
	/**
	 * 
	 * {根据域类型查询域的树结构}
	 * 
	 * @return

	 */
	public List<SecurityDomainVO> queryAllTreeByClass(String domaClass);

	public void updDomain4Drop(SecurityDomainVO tvo, int orderId, String point);
	
	public int checkSecurityIsExistsForDelete(String domaId) ;

}
