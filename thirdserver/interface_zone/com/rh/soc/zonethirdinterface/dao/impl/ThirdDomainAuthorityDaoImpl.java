package com.rh.soc.zonethirdinterface.dao.impl;

import org.springframework.jdbc.support.rowset.SqlRowSet;
import org.springframework.stereotype.Repository;

import com.rh.soc.zonethirdinterface.dao.IThirdDomainAuthorityDao;
import com.rh.webserver.common.base.dao.BaseDao;


@Repository("thirddomainauthoritydao")
public class ThirdDomainAuthorityDaoImpl extends BaseDao implements IThirdDomainAuthorityDao {

	/**
	 * 通过用户ID获取安全域
	 * @param personId   用户ID
	 * @return SqlRowSet 返回结果集
	 */
	public SqlRowSet getSecurityDomain(String personId) {
		StringBuilder sql = new StringBuilder(128);
		sql.append(" select uvg.grants_no domain_id, ");
		sql.append("        domain.doma_name domain_name ");
		sql.append(" from t_user_vs_grants uvg ");
		sql.append(" left join t_domain_dict domain on domain.doma_id = uvg.grants_code and domain.doma_class = 1 and domain.doma_status = 1 ");
		sql.append(" where  1 = 1 ");
		sql.append(" and  uvg.psn_id = ? ");

		return super.query(sql.toString(), personId);
	}
}
