package com.rh.soc.zonethirdinterface.dao;


import org.springframework.jdbc.support.rowset.SqlRowSet;

/**
 * 通用域DAO 当前只有安全域
 */
public interface IThirdDomainAuthorityDao {
	/**
	 * 通过用户ID获取安全域
	 * 
	 * @param personId   用户ID
	 * @return SqlRowSet 返回结果集
	 */
	public SqlRowSet getSecurityDomain(String personId);
}
