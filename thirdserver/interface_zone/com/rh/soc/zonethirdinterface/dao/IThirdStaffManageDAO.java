package com.rh.soc.zonethirdinterface.dao;

import java.util.List;

import com.rh.soc.management.usermanage.usermanage.model.UserInfoVO;
import com.rh.soc.securitydomainmanage.model.StaffInfoVO;
import com.rh.soc.securitydomainmanage.model.StaffMainInfoVO;


public interface IThirdStaffManageDAO {
	/**
	 * 查询组织id
	 * 
	 * @param domaId
	 * @return

	 * 
	 */
	public String querySSid(String domaId);

	/**
	 * 查询人员列表
	 * 
	 * @param staffInfoVO
	 * @return

	 */
	public List<StaffInfoVO> queryStaffList(StaffInfoVO staffInfoVO);

	/**
	 * 添加人员信息
	 * 
	 * @param staffInfoVO
	 * @return

	 */
	public int addStaff(StaffInfoVO staffInfoVO);

	/**
	 * 修改人员信息
	 * 
	 * @param staffInfoVO
	 * @return

	 */
	public int updStaff(StaffInfoVO staffInfoVO);

	/**
	 * 删除人员信息
	 * 
	 * @param staffId
	 * @return

	 */
	public int delStaff(String staffId);

	/**
	 * 登录系统选择是，向t_org_USER增加记录
	 * 
	 * @param vo
	 * @return String

	 */

	public String addUserInfo(StaffInfoVO vo) throws Exception;

	/**
	 * 如果某个人员登录系统为是，则查询出该人员的登录账号和密码
	 * 
	 * @param staffId
	 * @return

	 */
	public UserInfoVO queryLoginInfo(StaffInfoVO staffInfoVO);

	/**
	 * 如果某个人员登录系统由是改为否，则删除当前该人员在t_org_USER的记录
	 * 
	 * @param staffId
	 * @return

	 */
	public int delUserLoginInfo(String staffId);

	/**
	 * 查询登录用户详细信息
	 * 
	 * @param userInfoVO
	 * @return

	 */
	public UserInfoVO queryUserDetail(UserInfoVO userInfoVO);
	

	/**
	 * 查询roleId
	 * 
	 * @param staffInfoVO
	 * @return

	 */
	public void addRoleIdAndUserId(StaffInfoVO userInfoVO);

	/**
	 * 删除userId和roleId的关联表
	 * 
	 * @param staffInfoVO
	 * @return

	 */
	/*
	 * public void delUserAndRole(String staffId);
	 */
	/**
	 * 根据人员信息VO 查询出所有的表关联Id
	 * 
	 * @param staffInfoVO
	 * @return

	 */
	public List<StaffMainInfoVO> queryStaffMainInfoList(StaffMainInfoVO smvo);

	/**
	 * 根据用户id删除roleId和userId关联的表
	 * 
	 * @param staffId
	 * @return

	 */

	public void delUserInfo(String staffId);

	/**
	 * 根据userId删除角色Id和userId关联表的记录
	 * 
	 * @param staffInfoVO
	 * @return

	 */
	public int delRoleId(String userId);

	/**
	 * 如果某个人员登录系统为是，并且具有安全角色，则查询对应的安全角色Id
	 * 
	 * @param vo
	 * @return

	 */
	public List<StaffInfoVO> queryRoleId(StaffInfoVO staffInfoVO);

	/**
	 * 修改用户表信息
	 * 
	 * @param staffInfoVO
	 * @return

	 */
	public int updUser(StaffInfoVO staffInfoVO) throws Exception;

	/**
	 * 
	 * 判断改人员是否存在
	 * 
	 * @param staffId
	 * @return

	 */
	public int checkStaffExists(String staffId);

}
