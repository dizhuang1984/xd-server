package com.rh.soc.zonethirdinterface.controller;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.rh.soc.management.systemconfig.appconfig.model.AgentServerInfoVO;
import com.rh.soc.management.systemconfig.appconfig.service.IAppConfigService;
import com.rh.soc.securitydomainmanage.model.SecurityDomainVO;
import com.rh.soc.zonethirdinterface.model.AgentServerInfoInterfaceVO;
import com.rh.soc.zonethirdinterface.model.ZoneInfoInterfaceVO;
import com.rh.soc.zonethirdinterface.service.IThirdSecurityDomainService;
import com.rh.webserver.common.base.bean.AppResultBean;
import com.rh.webserver.common.base.controller.BaseController;
import com.rh.webserver.common.base.exception.AppException;
import com.rh.webserver.common.base.json.IDataCenter;
import com.rh.webserver.common.util.BeanUtils;
import com.rh.webserver.common.util.JsonUtil;

@Controller
@RequestMapping("/third/zonemanage")
public class ZoneThirdInterfaceController extends BaseController {
	private final Logger logger = Logger.getLogger(error);
	@Resource
	IThirdSecurityDomainService securityDomainIA;

	@Resource
	IAppConfigService appConfigService;

	/**
	 * 
	 * 管控域添加
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@ResponseBody
	@RequestMapping("addSave")
	public AppResultBean addSave(@RequestParam("param") String param, HttpSession session) throws Exception {
		logger.info("aparam=["+param+"]");
		IDataCenter idc = BeanUtils.toDataCenter(param);
		AppResultBean bean = new AppResultBean();

		ZoneInfoInterfaceVO zoneInfoInterfaceVO = (ZoneInfoInterfaceVO) BeanUtils.jsonToBean(idc, ZoneInfoInterfaceVO.class);
		SecurityDomainVO vo = changeCodeListToMailConfigVO(zoneInfoInterfaceVO);
		vo.setUserId(this.getUserId(session));
		vo.setDomaAbbreviation("");
		vo.getNodeId();
		try {
			SecurityDomainVO testvo = securityDomainIA.addSecurityDomain(vo);
			if (testvo.getFlag() == -9) {
				bean.fail("添加失败，管控域ID已经存在");
				return bean;
			}
			if (testvo.getFlag() == -10) {
				bean.fail("添加失败，管控域父ID不存在");
				return bean;
			}
//			if (testvo.getFlag() == -11) {
//				bean.fail("添加失败，代理ID不存在");
//				return bean;
//			}
			if (testvo.getFlag() == 6) {
				bean.fail("添加失败，您所选择的隶属管控域已经是第五层级，不可以在此管控域下再添加子管控域。");
				return bean;
			}
			if (testvo.getFlag() > 0) {
				bean.ok("添加管控域同步成功");
			} else {
				if (testvo.getFlag() == -2) {
					bean.fail("添加失败，管控域名称已存在。");
				} else if (testvo.getFlag() == -3) {
					bean.fail("添加失败，管控域简称已存在。");
				}
			}
		} catch (Exception e) {
			logger.error("管控域添加失败", e);
			bean.fail("添加失败。");
		}
		logger.info("addSave=>"+JsonUtil.write(bean));
		return bean;
	}

	/**
	 * 
	 * 修改管控域
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@ResponseBody
	@RequestMapping("updSave")
	public AppResultBean updSave(@RequestParam("param") String param, HttpSession session) throws Exception {
		logger.info("uparam=["+param+"]");
		IDataCenter idc = BeanUtils.toDataCenter(param);
		AppResultBean bean = new AppResultBean();

//		SecurityDomainVO vo = (SecurityDomainVO) BeanUtils.jsonToBean(idc, SecurityDomainVO.class);
		ZoneInfoInterfaceVO zoneInfoInterfaceVO = (ZoneInfoInterfaceVO) BeanUtils.jsonToBean(idc, ZoneInfoInterfaceVO.class);
		SecurityDomainVO vo = changeCodeListToMailConfigVO(zoneInfoInterfaceVO);
		
		vo.setDomaAbbreviation("");
		String ptempId = "";
		String newNodeId = null;
		try {
			SecurityDomainVO testvo = securityDomainIA.updateSecurityDomain(vo);
			if (testvo.getFlag() == 6) {
				bean.fail("修改失败，您所选择的隶属管控域已经是第五层级，不可以在此管控域下再添加子管控域。");
				return bean;
			}
			if (testvo.getFlag() == -1) {
				//bean.fail("修改失败，该管控域不存在。");
				//return bean;
				return addSave(param, session);
			}
			if (testvo.getFlag() == -2) {
				bean.fail("修改失败，管控域名称已存在。");
				return bean;
			}
			if (testvo.getFlag() == -3) {
				bean.fail("修改失败，简称已存在。");
				return bean;
			}
			if (testvo.getFlag() == -4) {
				bean.setMessage(testvo.getMessage(), -4);
				return bean;
			}

			if (testvo.getFlag() == -10) {
				bean.fail("修改失败，管控域父ID不存在");
				return bean;
			}

//			if (testvo.getFlag() == -11) {
//				bean.fail("修改失败，代理ID不存在");
//				return bean;
//			}

			if (testvo.getFlag() > 0) {
				bean.ok("修改管控域同步成功");
			}
		} catch (AppException e) {
			logger.error(e.getMessage(), e);
			bean.fail("修改失败。");
		}
		logger.info("updSave=>"+JsonUtil.write(bean));
		return bean;
	}
	
	
	/**
	 * 
	 * 修改管控域
	 * @throws Exception
	 */
	@ResponseBody
	@RequestMapping("updAndAddSave")
	public AppResultBean updAndAddSave(@RequestParam("param") String param, HttpSession session) throws Exception {
		logger.info("param=["+param+"]");
		IDataCenter idc = BeanUtils.toDataCenter(param);
		AppResultBean bean = new AppResultBean();

//		SecurityDomainVO vo = (SecurityDomainVO) BeanUtils.jsonToBean(idc, SecurityDomainVO.class);
		ZoneInfoInterfaceVO zoneInfoInterfaceVO = (ZoneInfoInterfaceVO) BeanUtils.jsonToBean(idc, ZoneInfoInterfaceVO.class);
		SecurityDomainVO vo = changeCodeListToMailConfigVO(zoneInfoInterfaceVO);
		
		vo.setDomaAbbreviation("");
		String ptempId = "";
		String newNodeId = null;
		try {
			SecurityDomainVO testvo = securityDomainIA.updAndAddSecurityDomain(vo);
			if (testvo.getFlag() == 6) {
				bean.fail("同步失败，您所选择的隶属管控域已经是第五层级，不可以在此管控域下再添加子管控域。");
				return bean;
			}
			if (testvo.getFlag() == -1) {
				bean.fail("同步失败，该管控域不存在。");
				return bean;
			}
			if (testvo.getFlag() == -2) {
				bean.fail("同步失败，管控域名称已存在。");
				return bean;
			}
			if (testvo.getFlag() == -3) {
				bean.fail("同步失败，简称已存在。");
				return bean;
			}
			if (testvo.getFlag() == -4) {
				bean.setMessage(testvo.getMessage(), -4);
				return bean;
			}

			if (testvo.getFlag() == -10) {
				bean.fail("同步失败，管控域父ID不存在");
				return bean;
			}

			if (testvo.getFlag() == -11) {
				bean.fail("同步失败，代理ID不存在");
				return bean;
			}

			if (testvo.getFlag() > 0) {
				bean.ok("同步管控域同步成功");
			}
		} catch (AppException e) {
			logger.error(e.getMessage(), e);
			bean.fail("同步失败。");
		}
		return bean;
	}
	
	
//	/**
//	 * 
//	 * 修改管控域
//	 * 
//	 * @param mapping
//	 * @param form
//	 * @param request
//	 * @param response
//	 * @return
//	 * @throws Exception
//	 */
//	@ResponseBody
//	@RequestMapping("updAndAddSave")
//	public AppResultBean updAndAddSave(@RequestParam("param") String param, HttpSession session) throws Exception {
//		logger.info("param=["+param+"]");
//		IDataCenter idc = BeanUtils.toDataCenter(param);
//		AppResultBean bean = new AppResultBean();
//
////		SecurityDomainVO vo = (SecurityDomainVO) BeanUtils.jsonToBean(idc, SecurityDomainVO.class);
//		ZoneInfoInterfaceVO zoneInfoInterfaceVO = (ZoneInfoInterfaceVO) BeanUtils.jsonToBean(idc, ZoneInfoInterfaceVO.class);
//		SecurityDomainVO vo = changeCodeListToMailConfigVO(zoneInfoInterfaceVO);
//		
//		vo.setDomaAbbreviation("");
//		String ptempId = "";
//		String newNodeId = null;
//		try {
//			SecurityDomainVO testvo = securityDomainIA.updAndAddSecurityDomain(vo);
//			if (testvo.getFlag() == 6) {
//				bean.fail("修改失败，您所选择的隶属管控域已经是第五层级，不可以在此管控域下再添加子管控域。");
//				return bean;
//			}
//			if (testvo.getFlag() == -1) {
//				bean.fail("修改失败，该管控域不存在。");
//				return bean;
//			}
//			if (testvo.getFlag() == -2) {
//				bean.fail("修改失败，管控域名称已存在。");
//				return bean;
//			}
//			if (testvo.getFlag() == -3) {
//				bean.fail("修改失败，简称已存在。");
//				return bean;
//			}
//			if (testvo.getFlag() == -4) {
//				bean.setMessage(testvo.getMessage(), -4);
//				return bean;
//			}
//
//			if (testvo.getFlag() == -10) {
//				bean.fail("修改失败，管控域父ID不存在");
//				return bean;
//			}
//
//			if (testvo.getFlag() == -11) {
//				bean.fail("修改失败，代理ID不存在");
//				return bean;
//			}
//
//			if (testvo.getFlag() > 0) {
//				bean.ok("修改管控域同步成功");
//			}
//		} catch (AppException e) {
//			logger.error("修改失败", e);
//			bean.fail("修改失败。");
//		}
//		return bean;
//	}

	/**
	 * 
	 * 删除管控域信息
	 * 
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@ResponseBody
	@RequestMapping("doDel")
	public AppResultBean doDel(@RequestParam("param") String param, HttpSession session) throws Exception {
		logger.info("param=["+param+"]");
		IDataCenter idc = BeanUtils.toDataCenter(param);

		AppResultBean bean = new AppResultBean();
		String domaId = idc.getParameter("sysId");
		String domaName = idc.getParameter("name");
		SecurityDomainVO vo = new SecurityDomainVO();
		vo.setDomaId(domaId);
		vo.setDomaName(domaName);
		vo.setDelFlag("1");// 表明为单个删除，如果为0表示为批量删除
		try {

			String rtn = securityDomainIA.deleteSecurityDomain(vo);

			if ("0".equals(rtn)) {
				bean.ok();
				bean.setMessage("删除管控域同步成功。");

			} else if ("-9".equals(rtn)) {
				bean.fail("管控域ID不存在");
			} else if ("-1".equals(rtn)) {
				bean.fail("管控域下存在备用资产，请删除资产后再删除管控域。");
			} else {
				bean.fail("管控域下存在资产，请删除资产后再删除管控域。");
			}

		} catch (AppException e) {
			logger.error(e.getMessage(), e);
			bean.fail("删除失败。");
		}
		return bean;
	}

	/**
	 * 修改页面调用 {初始化代理服务器信息}
	 */
	@ResponseBody
	@RequestMapping("queryAgentInitInfo")
	public AppResultBean queryAgentInitInfo(@RequestParam("param") String param, HttpSession session,
			HttpServletRequest request, HttpServletResponse response) throws Exception {
		logger.info("param=["+param+"]");
		IDataCenter idc = BeanUtils.toDataCenter(param);
		AppResultBean bean = new AppResultBean();
		AgentServerInfoVO vo = new AgentServerInfoVO();
		vo.setPageSize(99999999);
		vo.setCurrentPageNum(1);
		try {
			List<AgentServerInfoVO> agentInfoList = appConfigService.queryAllAgentServer(vo); // 代理服务器信息
			List<AgentServerInfoInterfaceVO> agentServerInfoInterfaceVOList = new ArrayList<AgentServerInfoInterfaceVO>();
			for (int i = 0; i < agentInfoList.size(); i++) {
				AgentServerInfoVO agentServerInfoVO = agentInfoList.get(i);
				AgentServerInfoInterfaceVO agentServerInfoInterfaceVO = new AgentServerInfoInterfaceVO();
				agentServerInfoInterfaceVO.setAppId(agentServerInfoVO.getAppId());
				agentServerInfoInterfaceVO.setNodeIpv(agentServerInfoVO.getNodeIpv());
				agentServerInfoInterfaceVO.setNodeName(agentServerInfoVO.getNodeName());
				agentServerInfoInterfaceVOList.add(agentServerInfoInterfaceVO);
			}
			bean.ok(agentServerInfoInterfaceVOList);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			bean.fail("获取数据失败");
		}
		return bean;
	}
	
	
	private SecurityDomainVO changeCodeListToMailConfigVO(ZoneInfoInterfaceVO zoneInfoInterfaceVO){
		SecurityDomainVO securityDomainVO=new SecurityDomainVO();
		securityDomainVO.setAgent_id(zoneInfoInterfaceVO.getAppId());
		securityDomainVO.setNodeId(zoneInfoInterfaceVO.getAppId());
		securityDomainVO.setPdomaId("-1");
		securityDomainVO.setDomaId(zoneInfoInterfaceVO.getSysId());
		securityDomainVO.setDomaName(zoneInfoInterfaceVO.getName());
		return securityDomainVO;
		
	}
	

}
