package com.rh.soc.zonethirdinterface.model;
import java.io.Serializable;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.rh.webserver.common.base.bean.BaseVO;

public class ZoneInfoInterfaceVO   extends BaseVO implements Serializable {

	/**  管控域Id */
	private String sysId;

	/** 管控域名称 */
	private String name;
	
	
	/** 代理ID */
	private String appId;

	/** networkCode(所属网络: 一类二类三类) */
	private String networkCode;
	
	/** networkName(所属网络: 一类二类三类) */
	private String networkName;
	
	/** 服务地址*/
	private String uri ;


	public String getSysId() {
		return sysId;
	}



	public void setSysId(String sysId) {
		this.sysId = sysId;
	}




	public String getName() {
		return name;
	}




	public void setName(String name) {
		this.name = name;
	}




	public String getNetworkCode() {
		return networkCode;
	}




	public void setNetworkCode(String networkCode) {
		this.networkCode = networkCode;
	}




	public String getUri() {
		return uri;
	}




	public void setUri(String uri) {
		this.uri = uri;
	}




	public String getAppId() {
		return appId;
	}



	public void setAppId(String appId) {
		this.appId = appId;
	}



	public String getNetworkName() {
		return networkName;
	}



	public void setNetworkName(String networkName) {
		this.networkName = networkName;
	}



	/**
	 * 
	 * {方法功能中文描述}
	 * 
	 * @param rs
	 * @param i
	 * @return
	 * @throws SQLException
	 */
	public Object mapRow(ResultSet rs, int i) throws SQLException {
		ZoneInfoInterfaceVO vo = new ZoneInfoInterfaceVO();
//		vo.setAppId(rs.getString("node_id"));
//		vo.setNodeName(rs.getString("node_name"));
//		try {
//			vo.setNodeDesc(rs.getString("node_desc"));
//		} catch (Exception e) {
//		}
//		vo.setNodeIpv(rs.getString("node_ipv"));
//		try {
//			vo.setScannerType(rs.getString("scancfg_no"));
//		} catch (Exception e) {
//		}
//		vo.setPreNodeIpv(vo.getNodeIpv());
//		vo.setRecordCount(super.getRecordCount());
		return vo;
	}

}
