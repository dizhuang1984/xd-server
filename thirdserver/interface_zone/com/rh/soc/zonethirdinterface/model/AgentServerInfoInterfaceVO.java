package com.rh.soc.zonethirdinterface.model;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.rh.webserver.common.base.bean.BaseVO;

public class AgentServerInfoInterfaceVO{

	/** 应用服务器ID */
	private String appId;

	/** 代理服务器名称 */
	private String nodeName;

	/** 代理服务器IP */
	private String nodeIpv;

	

	public String getAppId() {
		return appId;
	}



	public void setAppId(String appId) {
		this.appId = appId;
	}



	public String getNodeName() {
		return nodeName;
	}



	public void setNodeName(String nodeName) {
		this.nodeName = nodeName;
	}



	public String getNodeIpv() {
		return nodeIpv;
	}



	public void setNodeIpv(String nodeIpv) {
		this.nodeIpv = nodeIpv;
	}



	/**
	 * 
	 * {方法功能中文描述}
	 * 
	 * @param rs
	 * @param i
	 * @return
	 * @throws SQLException
	 */
	public Object mapRow(ResultSet rs, int i) throws SQLException {
		AgentServerInfoInterfaceVO vo = new AgentServerInfoInterfaceVO();
//		vo.setAppId(rs.getString("node_id"));
//		vo.setNodeName(rs.getString("node_name"));
//		try {
//			vo.setNodeDesc(rs.getString("node_desc"));
//		} catch (Exception e) {
//		}
//		vo.setNodeIpv(rs.getString("node_ipv"));
//		try {
//			vo.setScannerType(rs.getString("scancfg_no"));
//		} catch (Exception e) {
//		}
//		vo.setPreNodeIpv(vo.getNodeIpv());
//		vo.setRecordCount(super.getRecordCount());
		return vo;
	}

}
