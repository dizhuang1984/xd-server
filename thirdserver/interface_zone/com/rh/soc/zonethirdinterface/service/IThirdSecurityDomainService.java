package com.rh.soc.zonethirdinterface.service;

import java.util.List;

import com.rh.soc.bussinessdomainmanage.model.TreeVO;
import com.rh.soc.management.systemconfig.menuconfig.model.OperaVO;
import com.rh.soc.securitydomainmanage.model.SecurityDomainBriefVO;
import com.rh.soc.securitydomainmanage.model.SecurityDomainVO;
import com.rh.webserver.common.base.exception.AppException;

/**
 * 
 */
public interface IThirdSecurityDomainService {
	/**
	 * 
	 * 增加安全域信息
	 * @param SecurityDomainVO
	 * @return vo
	 */

	public SecurityDomainVO addSecurityDomain(SecurityDomainVO vo) throws AppException;

	/**
	 * 
	 * 查询安全域信息
	 * 
	 * @param SecurityDomainVO
	 * @return List

	 * 
	 */

	public List<SecurityDomainVO> queryAllSecurityDomain() throws AppException;

	/**
	 * 根据条件查询树形数据
	 */
	public List<String> queryTreeWithCondition(SecurityDomainBriefVO vo) throws AppException;

	/**
	 * 查询节点的子节点
	 * 
	 * @param vo
	 * @return
	 * @throws AppException
	 */
	public List<SecurityDomainBriefVO> queryTreeChildrenList(SecurityDomainBriefVO vo) throws AppException;

	/**
	 * 
	 * 删除安全域信息
	 * 
	 * @param SecurityDomainVO
	 * @return int

	 * 
	 */

	public String deleteSecurityDomain(SecurityDomainVO vo) throws AppException;

	/**
	 * 
	 * 更新安全域信息
	 * 
	 * @param SecurityDomainVO
	 * @return int

	 * 
	 */

	public SecurityDomainVO updateSecurityDomain(SecurityDomainVO vo) throws AppException;
	
	
	public SecurityDomainVO updAndAddSecurityDomain(SecurityDomainVO vo) throws AppException;

	/**
	 * 
	 * {修改需要互换位置的域信息}
	 * 
	 * @param list
	 * @return

	 * @throws AppException
	 */
	public void updDomaSort(OperaVO vo) throws AppException;

	/**
	 * 
	 * 查询安全域的详细信息
	 * 
	 * @param SecurityDomainVO
	 * @return vo

	 * 
	 */

	public SecurityDomainVO querySecurityDetail(String domaId) throws AppException;

	/**
	 * 
	 * 获取安全域树信息
	 * 
	 * @param vo
	 * @return
	 * @throws AppException

	 */
	public List<TreeVO> querySecurityDomainTree(TreeVO vo) throws AppException;

	/**
	 * 
	 * {验证域是否有子域}
	 * 
	 * @param domaId
	 * @return

	 * @throws AppException
	 */
	public String checkDomain(String domaId, String domaClass) throws AppException;

	/**
	 * 
	 * @return
	 * @throws AppException

	 */
	public String querySecurityDomainForHuaneng() throws AppException;

	/**
	 * 
	 * @param vo
	 * @return

	 * @throws AppException
	 */
	public String queryDomaList(String domaClass) throws AppException;
}
