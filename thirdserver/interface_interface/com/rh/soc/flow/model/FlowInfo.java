package com.rh.soc.flow.model;

import java.io.Serializable;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.rh.webserver.common.base.bean.BaseVO;

public class FlowInfo extends BaseVO implements Serializable {

	public String edId;
	public String interfaceId;
	public String interfaceDesc;
	public long in;
	public long out;
	public String time;

	// 开始
	public String start;
	// 结束
	public String end;

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public Object mapRow(ResultSet rs, int i) throws SQLException {
		FlowInfo vo = new FlowInfo();
		vo.edId = rs.getString("ed_id");
		vo.interfaceId = rs.getString("interface_ind");
		vo.in = rs.getLong("inV");
		vo.out = rs.getLong("outV");
		vo.time = rs.getString("t");
		vo.interfaceDesc = rs.getString("interface_desc");
		return vo;
	}

}
