package com.rh.soc.flow.model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

import com.rh.webserver.common.base.bean.BaseVO;

public class FlowQueryInfo extends BaseVO {

	public String edId;
	public String interfaceId;

	// 开始
	public String start;
	// 结束
	public String end;

	public Date t1;
	public Date t2;

	@Override
	public Object mapRow(ResultSet arg0, int arg1) throws SQLException {
		return null;
	}

}
