package com.rh.soc.flow.dao.impl;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.rh.soc.flow.dao.IFlowDao;
import com.rh.soc.flow.model.FlowInfo;
import com.rh.soc.flow.model.FlowQueryInfo;
import com.rh.soc.flow.model.FlowSumInfo;
import com.rh.webserver.common.base.dao.BaseDao;

@Repository
public class FlowDaoImpl extends BaseDao implements IFlowDao {

	@Override
	public List<FlowInfo> queryInterfaceInfoByEdId(FlowQueryInfo vo) {
		String sql = "select ed_id, interface_ind, interface_desc, avg(portal_flux) as inV, avg(export_flux) as outV, ";
		sql += " to_char( to_date(to_char(enter_date, 'yyyy-mm-dd HH24:mi'), 'yyyy-mm-dd HH24:mi') , 'yyyy-mm-dd HH24:mi') as t";
		sql += " From t_interface_info_bak where enter_date >= ? and enter_date <= ? ";
		sql += " and ed_id = ? ";
		sql += " group by ed_id, interface_ind, interface_desc, to_date(to_char(enter_date, 'yyyy-mm-dd HH24:mi'),'yyyy-mm-dd HH24:mi')";
		sql += " order by ed_id,interface_ind, to_date(to_char(enter_date, 'yyyy-mm-dd HH24:mi'),'yyyy-mm-dd HH24:mi') ";
		FlowInfo info = new FlowInfo();
		info.setIgnoreRecordLimit(true);
		info.setPageSize(Integer.MAX_VALUE);
		return simpleQuery(sql, info, new Object[] { vo.t1, vo.t2, vo.edId });
	}

	@Override
	public List<FlowSumInfo> queryInterfaceTotalFlowByEdId(FlowQueryInfo vo) {
		String sql = "select ed_id, interface_ind, interface_desc, sum(diffIn) as inV, sum(diffOut) as outV";
		sql += " From t_interface_info_bak where enter_date >= ? and enter_date <= ? ";
		sql += " and ed_id = ? ";
		sql += " group by ed_id, interface_ind, interface_desc";
		sql += " order by ed_id,interface_ind";
		FlowSumInfo info = new FlowSumInfo();
		info.setIgnoreRecordLimit(true);
		info.setPageSize(Integer.MAX_VALUE);
		return simpleQuery(sql, info, new Object[] { vo.t1, vo.t2, vo.edId });
	}

}
