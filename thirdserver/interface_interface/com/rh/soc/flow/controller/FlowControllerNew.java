package com.rh.soc.flow.controller;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.rh.soc.flow.model.FlowInfo;
import com.rh.soc.flow.model.FlowQueryInfo;
import com.rh.soc.flow.model.FlowSumInfo;
import com.rh.soc.flow.service.IFlowService;
import com.rh.webserver.common.base.bean.AppResultBean;
import com.rh.webserver.common.base.controller.BaseController;
import com.rh.webserver.common.base.json.IDataCenter;
import com.rh.webserver.common.util.BeanUtils;

@Controller
@RequestMapping("/third/queryInterfaceInfo")
public class FlowControllerNew extends BaseController {

	private Logger logger = Logger.getLogger(FlowControllerNew.class);

	@Resource
	private IFlowService INTERFACEINFOSERVICE;

	@ResponseBody
	@RequestMapping("queryInterfaceInfoByEdId")
	public AppResultBean queryInterfaceInfoByEdId(@RequestParam("param") String param, HttpSession session)
			throws Exception {
		AppResultBean bean = new AppResultBean();
		try {
			logger.info("queryInterfaceInfoByEdId -> " + param);
			IDataCenter idc = BeanUtils.toDataCenter(param);
			FlowQueryInfo vo = BeanUtils.jsonToBean(idc, FlowQueryInfo.class);
			Date tm1 = null;
			Date tm2 = null;
			if (vo.start == null || vo.end == null) {
				long t2 = System.currentTimeMillis();
				long t1 = System.currentTimeMillis() - 3600 * 1000;
				tm1 = new Date(t1);
				tm2 = new Date(t2);
			} else {
				SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
				tm2 = sdf.parse(vo.end);
				tm1 = sdf.parse(vo.start);
			}
			vo.t1 = tm1;
			vo.t2 = tm2;
			List<FlowInfo> list = INTERFACEINFOSERVICE.queryInterfaceInfoByEdId(vo);
			bean.ok(list);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			bean.fail("查询数据异常");
		}
		return bean;
	}

	@ResponseBody
	@RequestMapping("queryInterfaceTotalFlowByEdId")
	public AppResultBean queryInterfaceTotalFlowByEdId(@RequestParam("param") String param, HttpSession session)
			throws Exception {
		AppResultBean bean = new AppResultBean();
		try {
			logger.info("queryInterfaceTotalFlowByEdId -> " + param);
			IDataCenter idc = BeanUtils.toDataCenter(param);
			FlowQueryInfo vo = BeanUtils.jsonToBean(idc, FlowQueryInfo.class);
			Date tm1 = null;
			Date tm2 = null;
			if (vo.start == null || vo.end == null) {
				long t2 = System.currentTimeMillis();
				Calendar cal = Calendar.getInstance();
				cal.set(Calendar.HOUR_OF_DAY, 0);
				cal.set(Calendar.MINUTE, 0);
				cal.set(Calendar.SECOND, 0);
				cal.set(Calendar.MILLISECOND, 0);
				long t1 = cal.getTimeInMillis();
				tm1 = new Date(t1);
				tm2 = new Date(t2);
			} else {
				SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
				tm2 = sdf.parse(vo.end);
				tm1 = sdf.parse(vo.start);
			}
			vo.t1 = tm1;
			vo.t2 = tm2;
			List<FlowSumInfo> list = INTERFACEINFOSERVICE.queryInterfaceTotalFlowByEdId(vo);
			bean.ok(list);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			bean.fail("查询数据异常");
		}
		return bean;
	}
}
