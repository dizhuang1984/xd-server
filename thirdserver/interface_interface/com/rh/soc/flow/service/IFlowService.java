package com.rh.soc.flow.service;

import java.util.List;

import com.rh.soc.flow.model.FlowInfo;
import com.rh.soc.flow.model.FlowQueryInfo;
import com.rh.soc.flow.model.FlowSumInfo;

public interface IFlowService {

	List<FlowInfo> queryInterfaceInfoByEdId(FlowQueryInfo vo);

	List<FlowSumInfo> queryInterfaceTotalFlowByEdId(FlowQueryInfo vo);

}
