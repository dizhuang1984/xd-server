package com.rh.soc.flow.service.impl;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.rh.soc.flow.dao.IFlowDao;
import com.rh.soc.flow.model.FlowInfo;
import com.rh.soc.flow.model.FlowQueryInfo;
import com.rh.soc.flow.model.FlowSumInfo;
import com.rh.soc.flow.service.IFlowService;
import com.rh.webserver.common.base.service.BaseService;

@Service
public class FlowServiceImpl extends BaseService implements IFlowService {

	@Resource
	private IFlowDao dao;

	@Override
	public List<FlowInfo> queryInterfaceInfoByEdId(FlowQueryInfo vo) {
		return dao.queryInterfaceInfoByEdId(vo);
	}

	@Override
	public List<FlowSumInfo> queryInterfaceTotalFlowByEdId(FlowQueryInfo vo) {
		return dao.queryInterfaceTotalFlowByEdId(vo);
	}

}
