package com.rh.soc.remote.controller;

import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.rh.soc.comm.bean.Target;
import com.rh.soc.comm.center.ISysManager;
import com.rh.webserver.common.base.bean.AppResultBean;
import com.rh.webserver.common.base.controller.BaseController;
import com.rh.webserver.common.base.json.IDataCenter;
import com.rh.webserver.common.util.BeanUtils;

@Controller
@RequestMapping("third/remote")
public class RemoteController extends BaseController {

	@ResponseBody
	@RequestMapping("shutdown")
	public synchronized AppResultBean shutdown(@RequestParam(value = "param") String param, HttpSession session)
			throws Exception {
		AppResultBean bean = new AppResultBean();
		IDataCenter idc = BeanUtils.toDataCenter(param);
		String host = idc.getAsStr("ip");
		String port = idc.getAsStr("port");
		String userName = idc.getAsStr("username");
		String password = idc.getAsStr("password");
		Target t = new Target();
		t.assetIp = host;
		t.port = Integer.parseInt(port);
		t.loginUserName = userName;
		t.loginUserPass = password;
		boolean result = getRemoteInstance(ISysManager.class).shutdownHost(t, false);
		if (result) {
			bean.okmessage("关机成功");
		} else {
			bean.fail("关机失败");
		}
		return bean;
	}

	@ResponseBody
	@RequestMapping("restart")
	public synchronized AppResultBean restart(@RequestParam(value = "param") String param, HttpSession session)
			throws Exception {
		AppResultBean bean = new AppResultBean();
		IDataCenter idc = BeanUtils.toDataCenter(param);
		String host = idc.getAsStr("ip");
		String port = idc.getAsStr("port");
		String userName = idc.getAsStr("username");
		String password = idc.getAsStr("password");
		Target t = new Target();
		t.assetIp = host;
		t.port = Integer.parseInt(port);
		t.loginUserName = userName;
		t.loginUserPass = password;
		boolean result = getRemoteInstance(ISysManager.class).shutdownHost(t, true);
		if (result) {
			bean.okmessage("重启成功");
		} else {
			bean.fail("重启失败");
		}
		return bean;
	}

}
