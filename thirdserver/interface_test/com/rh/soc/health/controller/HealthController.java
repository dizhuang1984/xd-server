package com.rh.soc.health.controller;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.rh.soc.equipmentclassthirdinterface.service.impl.AsyncTest;
import com.rh.webserver.common.base.bean.AppResultBean;
import com.rh.webserver.common.base.controller.BaseController;

@Controller
@RequestMapping("/third/health")
public class HealthController extends BaseController {

	@Autowired AsyncTest t;
	
	@ResponseBody
	@RequestMapping("test")
	public synchronized AppResultBean addEquipmentTypeSave(HttpSession session) throws Exception {
		AppResultBean bean = new AppResultBean();
		bean.put("status", "ok");
		t.test();
		return bean;
	}

}
