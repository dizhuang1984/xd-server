package com.rh.soc.assetthirdinterface.model;

import java.sql.ResultSet;
import java.sql.SQLException;

import com.rh.soc.assetmanage.model.AssetVO;
import com.rh.webserver.common.base.bean.BaseVO;

public class AssetInfoVO extends BaseVO implements Cloneable {

	// /**
	// * 设备标识
	// */
	// public String edId;
	//
	// /**
	// * 设备名称
	// */
	// public String assetName;
	//
	// /**
	// * 设备类型
	// */
	// public String assetTypeId;
	//
	// /**
	// * 操作系统
	// */
	// public String osType;
	//
	// /**
	// * 系统标识
	// */
	// public String securityDomainId;
	//
	// /**
	// * 设备描述
	// */
	// public String assetDesc;
	//
	// /**
	// * 厂家
	// */
	// public String supplierId;
	//
	// /**
	// * 产品型号
	// */
	// public String assetModel;
	//
	// /**
	// * 硬件版本
	// */
	// public String hardwareVersion;
	//
	// /**
	// * 固件版本
	// */
	// public String firmwareVersion;
	//
	// /**
	// * 供应商
	// */
	// public String contractorsCode;
	//
	// /**
	// * IP
	// */
	// public String mainIp;
	//
	// /**
	// * 物理地址
	// */
	// public String location;
	//
	// /**
	// * 购置日期
	// */
	// public String buyDate;
	//
	// /**
	// * 资产价值
	// */
	// public String assetValue;
	//
	// public Object mapRow(ResultSet rs, int i) throws SQLException {
	// AssetInfoVO vo = new AssetInfoVO();
	// return vo;
	// }
	//
	// public String getEdId() {
	// return edId;
	// }
	//
	// public void setEdId(String edId) {
	// this.edId = edId;
	// }
	//
	// public String getAssetName() {
	// return assetName;
	// }
	//
	// public void setAssetName(String assetName) {
	// this.assetName = assetName;
	// }
	//
	// public String getAssetTypeId() {
	// return assetTypeId;
	// }
	//
	// public void setAssetTypeId(String assetTypeId) {
	// this.assetTypeId = assetTypeId;
	// }
	//
	// public String getOsType() {
	// return osType;
	// }
	//
	// public void setOsType(String osType) {
	// this.osType = osType;
	// }
	//
	// public String getSecurityDomainId() {
	// return securityDomainId;
	// }
	//
	// public void setSecurityDomainId(String securityDomainId) {
	// this.securityDomainId = securityDomainId;
	// }
	//
	// public String getAssetDesc() {
	// return assetDesc;
	// }
	//
	// public void setAssetDesc(String assetDesc) {
	// this.assetDesc = assetDesc;
	// }
	//
	// public String getSupplierId() {
	// return supplierId;
	// }
	//
	// public void setSupplierId(String supplierId) {
	// this.supplierId = supplierId;
	// }
	//
	// public String getAssetModel() {
	// return assetModel;
	// }
	//
	// public void setAssetModel(String assetModel) {
	// this.assetModel = assetModel;
	// }
	//
	// public String getHardwareVersion() {
	// return hardwareVersion;
	// }
	//
	// public void setHardwareVersion(String hardwareVersion) {
	// this.hardwareVersion = hardwareVersion;
	// }
	//
	// public String getFirmwareVersion() {
	// return firmwareVersion;
	// }
	//
	// public void setFirmwareVersion(String firmwareVersion) {
	// this.firmwareVersion = firmwareVersion;
	// }
	//
	// public String getContractorsCode() {
	// return contractorsCode;
	// }
	//
	// public void setContractorsCode(String contractorsCode) {
	// this.contractorsCode = contractorsCode;
	// }
	//
	// public String getMainIp() {
	// return mainIp;
	// }
	//
	// public void setMainIp(String mainIp) {
	// this.mainIp = mainIp;
	// }
	//
	// public String getLocation() {
	// return location;
	// }
	//
	// public void setLocation(String location) {
	// this.location = location;
	// }
	//
	// public String getBuyDate() {
	// return buyDate;
	// }
	//
	// public void setBuyDate(String buyDate) {
	// this.buyDate = buyDate;
	// }
	//
	// public String getAssetValue() {
	// return assetValue;
	// }
	//
	// public void setAssetValue(String assetValue) {
	// this.assetValue = assetValue;
	// }
	
	
	
	
	/**
	 * 设备标识
	 */
	public String deviceId;

	/**
	 * 设备名称
	 */
	public String deviceName;

	/**
	 * 设备类型
	 */
	public String type;

	/**
	 * 操作系统
	 */
	public String os;

	/**
	 * 系统标识
	 */
	public String systemId;

	/**
	 * 设备描述
	 */
	public String description;

	/**
	 * 厂家
	 */
	public String manufactory;

	/**
	 * 产品型号
	 */
	public String productType;

	/**
	 * 硬件版本
	 */
	public String hardwareVersion;

	/**
	 * 固件版本
	 */
	public String firmwareVersion;

	/**
	 * 供应商
	 */
	public String vendor;

	/**
	 * IP
	 */
	public String ip;

	/**
	 * 物理地址
	 */
	public String mac;

	/**
	 * 购置日期
	 */
	public String buyTime;

	/**
	 * 资产价值
	 */
	public String deviceValue;

	@Override
	public Object mapRow(ResultSet arg0, int arg1) throws SQLException {
		// TODO Auto-generated method stub
		return null;
	}

	public void setDeviceId(String deviceId) {
		this.deviceId = deviceId;
	}

	public void setDeviceName(String deviceName) {
		this.deviceName = deviceName;
	}

	public void setType(String type) {
		this.type = type;
	}

	public void setOs(String os) {
		this.os = os;
	}

	public void setSystemId(String systemId) {
		this.systemId = systemId;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public void setManufactory(String manufactory) {
		this.manufactory = manufactory;
	}

	public void setProductType(String productType) {
		this.productType = productType;
	}

	public void setHardwareVersion(String hardwareVersion) {
		this.hardwareVersion = hardwareVersion;
	}

	public void setFirmwareVersion(String firmwareVersion) {
		this.firmwareVersion = firmwareVersion;
	}

	public void setVendor(String vendor) {
		this.vendor = vendor;
	}

	public void setIp(String ip) {
		this.ip = ip;
	}

	public void setMac(String mac) {
		this.mac = mac;
	}

	public void setBuyTime(String buyTime) {
		this.buyTime = buyTime;
	}

	public void setDeviceValue(String deviceValue) {
		this.deviceValue = deviceValue;
	}

	public String getDeviceId() {
		return deviceId;
	}

	public String getDeviceName() {
		return deviceName;
	}

	public String getType() {
		return type;
	}

	public String getOs() {
		return os;
	}

	public String getSystemId() {
		return systemId;
	}

	public String getDescription() {
		return description;
	}

	public String getManufactory() {
		return manufactory;
	}

	public String getProductType() {
		return productType;
	}

	public String getHardwareVersion() {
		return hardwareVersion;
	}

	public String getFirmwareVersion() {
		return firmwareVersion;
	}

	public String getVendor() {
		return vendor;
	}

	public String getIp() {
		return ip;
	}

	public String getMac() {
		return mac;
	}

	public String getBuyTime() {
		return buyTime;
	}

	public String getDeviceValue() {
		return deviceValue;
	}
	
	
	
	
}